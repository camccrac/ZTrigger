"""
A module to download the datasets you need to make scale factors.

First you need to figure out which datasets to download,
i.e. which version of the NTuples you need. Use constants module to store that.

Tips:
- Search email to see if Davide has announced any new productions.
- Search on the indico page of the weekly TP roundtable.
- We're looking for complete productions.
- Check for tags in gitlab, e.g. search for 65.0.10 or R22 or something.
- Check in InputConfMPI, Davide makes those and sometimes they're named nicely.
"""
from __future__ import print_function
from subprocess import Popen, PIPE, STDOUT
import core.constants as c


def get_rucio_datasets(pattern):
    """Given a pattern to match, get rucio datasets."""
    cmd = ["rucio", "list-dids", pattern]
    process = Popen(cmd, stdout=PIPE, stderr=STDOUT)
    stdout, stderr = process.communicate()
    datasets = []

    if stderr is not None:
        raise ValueError(stderr)

    lines = str(stdout).splitlines()
    for line in lines:
        words = line.split()
        for word in words:
            if "group.perf-muons:group.perf-muons" in word:
                datasets.append(word)
    return datasets

def main():
    """Download rucio datasets"""
    print("Getting dataset names...")
    datasets = []

    for year in c.YEARS:
        print(year)
        short_year = str(year)[2:]  # e.g. 18 from 2018
        # get data datasets
        print("\tdata")
        data_pattern = "group.perf-muons:group.perf-muons.data" + short_year +\
            "_13TeV*physics_Main*" + c.NTUPLE_VERSION +"*EXT0"
        datasets += get_rucio_datasets(data_pattern)
        print("\tmc")
        r_tag = c.R_TAGS[c.MC_CAMPAIGNS[short_year]]
        mc_pattern = "group.perf-muons:group.perf-muons.mc*Powheg*Zmumu*" +\
            r_tag + "*" + c.NTUPLE_VERSION +"*EXT0"
        datasets += get_rucio_datasets(mc_pattern)

    print("Creating rucio rules (downloading datasets)...")
    print("If you see 'A duplicate rule, ... already exists, that's fine.")
    print("It just means someone must have already downloaded it.")
    for dataset in datasets:
        cmd = [
            "rucio", "add-rule",
            "--lifetime", "31104000",  # 365 days
            "--grouping", "ALL",  # not sure if this is strictly necessary
            dataset,
            "1",  # make 1 copy
            "CA-SFU-T2_LOCALGROUPDISK"  # or another rse if you want
        ]
        process = Popen(cmd, stdout=PIPE, stderr=STDOUT)
        stdout, stderr = process.communicate()
        if stderr is not None:
            raise ValueError(stderr)
        print(str(stdout))


if __name__ == "__main__":
    main()
