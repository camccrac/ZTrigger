"""To check match histos in 2015"""
import os
import uproot
import core.constants as c
import core.run_numbers as run_numbers
import core.triggers as triggers

# these are all the ones from 2015
root_filenames = [
    "group.perf-muons/group.perf-muons.29690601.EXT0._000529.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000528.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000517.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000523.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000524.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000500.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000501.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000527.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000467.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000476.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000504.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000515.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000516.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000526.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000313.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000319.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000461.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000462.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000481.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000525.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000288.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000285.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000286.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000297.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000298.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000314.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000315.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000336.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000337.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000338.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000339.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000340.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000396.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000402.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000459.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000468.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000477.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000483.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000484.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000257.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000496.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000497.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000243.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000486.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000487.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000488.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000489.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000490.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000491.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000492.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000493.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000210.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000211.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000212.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000213.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000214.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000215.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000216.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000217.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000218.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000207.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000208.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000209.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000174.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000192.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000193.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000275.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000436.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000139.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000123.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000124.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000125.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000280.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000281.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000282.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000292.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000293.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000320.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000323.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000474.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000479.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000120.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000121.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000122.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000318.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000322.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000469.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000095.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000096.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000097.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000098.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000126.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000127.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000128.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000129.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000130.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000131.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000132.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000133.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000169.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000170.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000173.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000176.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000177.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000221.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000222.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000223.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000224.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000225.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000226.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000227.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000231.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000232.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000233.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000234.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000235.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000236.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000237.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000238.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000239.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000240.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000307.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000308.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000310.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000342.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000343.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000344.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000345.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000346.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000347.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000348.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000349.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000350.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000351.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000352.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000353.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000354.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000355.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000356.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000357.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000358.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000359.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000377.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000378.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000379.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000395.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000083.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000104.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000166.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000178.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000242.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000252.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000256.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000284.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000081.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000082.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000084.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000105.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000106.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000188.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000480.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000482.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000519.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000079.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000134.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000171.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000186.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000219.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000220.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000229.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000325.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000374.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000503.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000507.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000514.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000076.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000107.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000108.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000109.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000110.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000111.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000112.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000113.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000114.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000148.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000149.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000241.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000057.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000064.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000066.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000067.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000068.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000069.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000093.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000099.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000115.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000116.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000135.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000136.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000137.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000138.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000141.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000142.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000143.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000144.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000145.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000146.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000147.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000172.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000181.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000185.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000194.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000203.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000204.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000205.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000206.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000230.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000301.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000302.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000399.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000502.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000509.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000510.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000511.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000512.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000041.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000080.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000088.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000090.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000092.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000094.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000103.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000140.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000034.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000042.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000365.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000426.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000429.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000019.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000020.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000021.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000040.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000058.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000070.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000071.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000072.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000073.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000074.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000151.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000175.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000196.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000197.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000198.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000199.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000200.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000202.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000228.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000244.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000505.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000508.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000012.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000065.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000075.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000077.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000078.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000255.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000277.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000279.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000294.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000295.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000296.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000299.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000309.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000311.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000312.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000316.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000326.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000341.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000366.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000376.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000381.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000382.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000383.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000384.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000385.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000392.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000393.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000394.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000420.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000421.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000422.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000423.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000424.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000427.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000428.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000430.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000435.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000494.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000003.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000004.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000002.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000460.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000475.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000478.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000506.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000518.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000001.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000091.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000100.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000101.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000102.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000360.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000361.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000362.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000363.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000364.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000372.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000373.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000375.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000380.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000397.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000398.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000400.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000401.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000403.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000404.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000405.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000406.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000407.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000408.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000409.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000410.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000412.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000413.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000414.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000415.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000416.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000417.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000418.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000419.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000425.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000431.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000432.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000433.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000438.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000440.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000441.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000442.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000443.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000444.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000445.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000446.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000447.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000448.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000449.NTUP_MCPTP.root",
    "group.perf-muons/group.perf-muons.29690601.EXT0._000450.NTUP_MCPTP.root",
    ]

cedar_path = \
    "/home/callum/projects/ctb-stelzer/callum/v66.3.0_MuonTPP_R22/run/"

for file_name in root_filenames:
    if os.path.exists(file_name):
        continue
    file_path = cedar_path + file_name
    os.system(
        "scp callum@cedar.computecanada.ca:"+file_path+\
        " ./group.perf-muons/")

print("Checking in all", len(root_filenames), "files for 2015")

for root_filename in root_filenames:
    print("Checking", root_filename)
    assert os.path.exists(root_filename)

    root_file = uproot.open(root_filename)

    run_number = root_file["MetaDataTree/runNumber"]
    min_run_num = min(run_number.array())
    max_run_num = max(run_number.array())

    # find which year + period this is
    file_year = None
    file_period = None
    for year in c.YEARS:
        periods = run_numbers.get_periods(year, sort=True)
        for period in periods:
            start_num = run_numbers.RUN_NUMBERS[year][period]["START"]
            end_num = run_numbers.RUN_NUMBERS[year][period]["END"]
            if start_num <= min_run_num <= max_run_num <= end_num:
                file_year, file_period = year, period
                print("File corresponds to period:", file_year, file_period)
                break
    if file_year is None and file_period is None:
        print("File is not in one of the periods we care about, skip!")
        continue

    # get all triggers
    trigs = triggers.triggers_in_period(
        single=True, year=file_year, period=file_period) +\
        triggers.triggers_in_period(
            single=False, year=file_year, period=file_period)

    # ignore ORs
    trigs = [trig for trig in trigs if "_OR_" not in trig]

    ttree = root_file["ZmumuTPMerged/Trees/MuonProbes_OC/TPTree_MuonProbes_OC"]
    #print("Looking in this tree:", ttree)
    # we want to check if probe_matched_HLT_xxx exists as a branch of ttree
    has_errors = False
    for trigger in trigs:
        # get branch
        try:
            branch_name = "probe_matched_"+trigger
            branch = ttree[branch_name]
        except ReferenceError:
            print("Trigger branch not found:", trigger)
            has_errors = True
            continue
        # check if it has any entries
        entries = branch.array()
        n_entries = len(entries)
        if n_entries == 0:
            print("no entries for histo:", branch_name)
            has_errors = True
        else:
            # check that it has at least one non-zero entry
            n_0 = len(entries[entries==0])
            n_1 = len(entries[entries==1])
            assert len(entries) == n_0+n_1
            if n_1 == 0:
                print("branch only has zero entries:", branch_name)
                has_errors = True
    if not has_errors:
        print("No errors detected here")
