"""Module to check that all the triggers you want exist as ntuple branches."""
from __future__ import print_function
from subprocess import Popen, PIPE, STDOUT, check_output
from os.path import join, exists
import ROOT as root
import core.constants as c
import core.run_numbers as run_numbers
import core.triggers as triggers
from filter_input_files import DEFAULT_FILTERED_FILENAME

# by default filter_input_files saves in this spot:
DEFAULT_FILE = join(c.RUN_DIR, DEFAULT_FILTERED_FILENAME)


def download_files(filtered_filename=DEFAULT_FILE, save_filename="paths.txt"):
    """
    Using the filtered.txt file (or similar), which contains rucio datasets,
    list the root files in that dataset and download the first one.
    """
    # read from file if it exists
    if exists(save_filename):
        with open(save_filename, "r+") as paths_file:
            saved_paths = paths_file.readlines()
            return saved_paths

    # Otherwise...

    # get list of ntuples
    with open(filtered_filename) as filtered_file:
        dataset_names = filtered_file.readlines()

    saved_paths = []

    for dataset_name in dataset_names:
        print("Using rucio dataset", dataset_name)

        # rucio list-files dataset_name will list a bunch of files like
        # +------------+------+---------+----------+--------+
        # | SCOPE:NAME | GUID | ADLER32 | FILESIZE | EVENTS |
        # |------------+------+---------+----------+--------|
        # | abc01.root | .... | ....... | ........ | ...... |
        # | abc02.root | .... | ....... | ........ | ...... |

        # get the first file (again assuming they all have the same branches)
        print("Listing files...")
        ls_cmd_output = check_output("rucio list-files "+dataset_name, shell=True)
        print("Result:")
        print(ls_cmd_output)

        # download all of them
        words = ls_cmd_output.split()
        root_files = [word for word in words if ".root" in word]
        print("Found", len(root_files), "ROOT files")

        for root_file in root_files:

            # root_file = "group.perf-muons:group.xxxxx.root"
            # should be saved as group.perf-muons/group.xxxxx.root
            # now open the file:
            save_path = join(*root_file.replace(":", " ").split())
            if not exists(save_path):
                print("Downloading", root_file)
                download_cmd = ["rucio", "download", root_file]
                dl_process = Popen(download_cmd, stdout=PIPE, stderr=STDOUT)
                dl_stdout, dl_stderr = dl_process.communicate()
                if dl_stderr is not None:
                    raise ValueError(dl_stderr)
                dl_cmd_output = str(dl_stdout)
                print(dl_cmd_output)

                print("File should be asved as", save_path)
            saved_paths.append(save_path)
    # save paths to a file for later
    with open(save_filename, 'w+') as paths_file:
        paths_file.writelines(saved_paths)

    return saved_paths

def check(root_filenames=None):
    """Check that the ntuples in filtered_filename have the right branches."""
    if root_filenames is None:
        root_filenames = download_files()

    print("Using", len(root_filenames), "files")

    # dict to store data about which triggers are not found by period
    trigs_not_found = {1692: {"A": ["list", "of", "missing", "triggers"]}}

    for root_filename in root_filenames:
        print("Checking in", root_filename)
        root_file = root.TFile(root_filename)

        metadata = root_file.Get("MetaDataTree")
        min_run_num = metadata.GetMinimum("runNumber")
        max_run_num = metadata.GetMaximum("runNumber")

        # find which year + period this is
        file_year = None
        file_period = None
        for year in c.YEARS:
            periods = run_numbers.get_periods(year, sort=True)
            for period in periods:
                start_num = run_numbers.RUN_NUMBERS[year][period]["START"]
                end_num = run_numbers.RUN_NUMBERS[year][period]["END"]
                if start_num <= min_run_num <= max_run_num <= end_num:
                    file_year, file_period = year, period
                    print("found period:", file_year, file_period)
                    break
        if file_year is None and file_period is None:
            print(
                "Skipping file with",
                root_file.Get("MetaDataTree").GetEntries(),
                "entries")
            continue

        # get all triggers
        trigs = triggers.triggers_in_period(
            single=True, year=file_year, period=file_period) +\
            triggers.triggers_in_period(
                single=False, year=file_year, period=file_period)

        # ignore ORs
        trigs = [trig for trig in trigs if "_OR_" not in trig]
        print("Will check", len(trigs), "triggers")

        ttree = root_file.Get(
            "ZmumuTPMerged/Trees/MuonProbes_OC/TPTree_MuonProbes_OC")
        print("Looking in this tree:", ttree)
        # we want to check if probe_dRMatch_HLT_xxx exists as a branch of ttree
        not_found = []
        for trigger in trigs:
            print("Checking", trigger)
            try:
                branch_name = "probe_dRMatch_"+trigger
                ttree.GetBranch(branch_name).GetName()
            except ReferenceError:
                not_found.append(trigger)

        # check that this matches other files from this period
        if file_year not in trigs_not_found.keys():
            trigs_not_found[file_year] = {}

        if file_period not in trigs_not_found[file_year].keys():
            trigs_not_found[file_year][file_period] = not_found
        else:
            if trigs_not_found[file_year][file_period] != not_found:
                print(trigs_not_found[file_year][file_period])
                print(not_found)
                raise ValueError("Found conflict!")



if __name__ == "__main__":
    check()
