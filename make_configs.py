"""
A script to show how I made run2 configs.

Hopefully this will make it a little easier to do run3 things?
"""
from __future__ import print_function
import core.constants as c
from core.run_numbers import get_periods
from config_makers.nominal_config import make_nominal_config
from config_makers.create_dirs import create_dirs
from config_makers.basic_config import make_basic_config
from config_makers.matches_config import make_match_configs
from config_makers.variation_config import make_variation_config
from config_makers.detector_regions_config import make_detector_regions_config

def main():
    """Make all the required config files for WTPH"""
    create_dirs()

    # make detector regions file
    make_detector_regions_config()

    for year in c.YEARS:
        for period in get_periods(year):
            # make trigger match files
            single_match_config = make_match_configs(year, period, single=True)
            multi_match_config = make_match_configs(year, period, single=False)

            # make basic config file
            single_basic_filename = make_basic_config(
                single_match_config, year, period, single=True)
            multi_basic_filename = make_basic_config(
                multi_match_config, year, period, single=False)

            # make nominal config for Data/MC & Single/Multi
            single_nominal_conf_data = make_nominal_config(
                single=True, year=year, period=period,
                basic_filename=single_basic_filename, data=True)
            multi_nominal_conf_data = make_nominal_config(
                single=False, year=year, period=period,
                basic_filename=multi_basic_filename, data=True)
            single_nominal_conf_mc = make_nominal_config(
                single=True, year=year, period=period,
                basic_filename=single_basic_filename, data=False)
            multi_nominal_conf_mc = make_nominal_config(
                single=False, year=year, period=period,
                basic_filename=multi_basic_filename, data=False)

            for variation in c.VARIATIONS[c.NTUPLE_VERSION]:
                # make variation file
                print(year, period, variation)

                # data
                make_variation_config(
                    single_nominal_conf_data, variation, year, period,
                    single=True, data=True)
                make_variation_config(
                    multi_nominal_conf_data, variation, year, period,
                    single=False, data=True)
                # mc
                make_variation_config(
                    single_nominal_conf_mc, variation, year, period,
                    single=True, data=False)
                make_variation_config(
                    multi_nominal_conf_mc, variation, year, period,
                    single=False, data=False)

if __name__ == "__main__":
    main()
