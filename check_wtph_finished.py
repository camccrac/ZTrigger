"""To help ensure all wtph output files exist."""

from __future__ import print_function
from os.path import join, exists
import os
import itertools
from ROOT import TFile
import core.constants as c
from core.run_numbers import get_periods
from core.triggers import triggers_in_period
from core.supported_triggers import is_supported

RERUN_FILE = join(c.WTPH_JOB_AREA, "CleanUpWTPH.sh")
OUTPUT_FILE = os.path.join(c.RUN_DIR, "trigger_table.csv")

def make_trigger_table(problems):
    """
    Make a CSV file of supported triggers / did it work in R21 vs R22.

    r21_problems = {2017: {"B": "HLT_muXXXX"}}, triggers with problems in R21
    r22_problems = {2017: {"B": "HLT_muXXXX"}}, triggers with problems in R22
    """
    def works(problems_dict, year, period, trigger):
        """Return whether this trigger ran properly, based on problems_dict."""
        if year not in problems_dict:
            return True
        elif period not in problems_dict[year]:
            return True
        elif trigger not in problems_dict[year][period]:
            return True
        else:
            return False

    text = "YEAR,PERIOD,TRIGGER,SUPPORTED,WORKS,EMPTY_SYSTS,EMPTY_DATA_MC," +\
        "EMPTY_REGIONS,EMPTY_QUALITIES,EMPTY_HISTO_TYPES\n"
    for year in c.YEARS:
        for period in get_periods(year, sort=True):
            single_trigs = triggers_in_period(True, year, period)
            multi_trigs = triggers_in_period(False, year, period)
            for trigger in single_trigs + multi_trigs:
                supported = is_supported(year, period, trigger)
                run22works = works(problems, year, period, trigger)
                if run22works:
                    details = ",,,,"
                else:
                    probs = problems[year][period][trigger]
                    empty_systs = probs["systematics"]
                    empty_data_mc = probs["data/mc"]
                    empty_regions = probs["regions"]
                    empty_qualities = probs["qualities"]
                    empty_hist_types = probs["types"]

                    if set(empty_systs) == set(c.VARIATIONS[c.NTUPLE_VERSION]):
                        empty_systs = "ALL"
                    elif len(empty_systs) == 1:
                        empty_systs = empty_systs[0]
                    else:
                        empty_systs = ";".join(empty_systs)

                    if len(empty_data_mc) == 1:
                        empty_data_mc = empty_data_mc[0]
                    else:
                        empty_data_mc = "both"

                    if set(empty_regions) == set(c.DETECTOR_REGIONS):
                        empty_regions = "ALL"
                    elif len(empty_regions) == 1:
                        empty_regions = empty_regions[0]
                    else:
                        empty_regions = ";".join(empty_regions)

                    if set(empty_qualities) == set(c.WORKING_POINTS):
                        empty_qualities = "ALL"
                    elif len(empty_qualities) == 1:
                        empty_qualities = empty_qualities[0]
                    else:
                        empty_qualities = ";".join(empty_qualities)

                    if len(empty_hist_types) == 1:
                        empty_hist_types = empty_hist_types[0]
                    else:
                        empty_hist_types = ";".join(empty_hist_types)

                    details = ",".join([
                        empty_systs, empty_data_mc, empty_regions,
                        empty_qualities, empty_hist_types])
                # add to the text we'll output to the csv
                text += ",".join([
                    str(year), period, trigger,
                    str(supported),
                    str(run22works), details])+"\n"

    with open(OUTPUT_FILE, "w") as out_f:
        out_f.write(text)
    print("Trigger table saved to", OUTPUT_FILE)

def parse_wtph_output_filename(filename):
    """
    E.g. given "data2018_D_ptdw_v66.3.0_MultiLegTriggers_1D.root"

    Return "data", "2018", "D", "ptdw", "v66.3.0", "MultiLegTriggers"
    """
    filename = str(filename) # I'd type-hint this but we need to use py2
    if filename.startswith("data"):
        data_mc = "data"
    else:
        data_mc = "mc"
    # cut off first couple characters
    filename = filename[len(data_mc):]
    # cut off .root
    filename = filename[:-len(".root")-1]

    # get the year, cut filename down a little
    year = filename.split("_")[0]
    filename = filename[len(year)+1:]

    # get period
    period = filename.split("_")[0]
    filename = filename[len(period)+1:]

    # get dimension from the end
    dim = filename.split("_")[-1]
    filename = filename[:-len(dim)-1]

    # get dimension from the end
    trigger_type = filename.split("_")[-1]
    filename = filename[:-len(trigger_type)-1]

    # get version from the end
    version = filename.split("_")[-1]
    filename = filename[:-len(version)-1]

    # all that's left is the variation name
    variation = filename

    return data_mc, year, period, variation, version, trigger_type

def parse_histo(histo_name):
    """
    Parse a histogram name (i.e. the ones from inside the WTPH files).

    Something like:
    ZmumuTPMerged/OC/HLT_mu6_msonly/Match/
    MediumMuonProbes_Barrel_OC_HLT_mu6_msonly_Match_etaphi_fine_Barrel
    """
    # I'd type hint this but nope py2
    histo_name = str(histo_name)
    path_parts = histo_name.split("/")
    # 0th one is always ZmumuTPMerged
    # 1th is redundant with histo name
    # 2th is always OC
    # 3th is trigger_name
    trigger_name = path_parts[3]
    # 4th is Probe or Match
    histo_type = path_parts[4]
    # 5th is the name of the histo
    histo_name = path_parts[5]
    # example histo_name:
    # MediumMuonProbes_Barrel_OC_HLT_mu6_msonly_Match_etaphi_fine_Barrel
    histo_name = histo_name.replace("MuonProbes", "")
    histo_name = histo_name.replace("OC_", "")
    # only take stuff before _histo_type_
    histo_name = histo_name.split("_"+histo_type+"_", 1)[0]

    quality = histo_name.split("_")[0]
    histo_name = histo_name.replace(quality+"_", "")

    region = histo_name.split("_")[0]
    histo_name = histo_name.replace(region+"_", "")

    return histo_type, quality, region, trigger_name

def histo_problems(single, year, period, wtph_out_file):
    """
    Check all required histograms in a WTPH output file are non-empty.

    If everything's OK, return None.

    Otherwise return branches_missing, exist_but_no_entries (both lists).
    """

    # open WTPH output file
    root_file = TFile(wtph_out_file)
    # check that histograms exist for all
    # qualities / regions / triggers
    triggers = triggers_in_period(single, year, period)

    # assume files are names like [...]_2D.root or _1D.root
    dim = "2D" if wtph_out_file.endswith("_2D.root") else "1D"

    branches_missing = []
    exist_but_no_entries = []
    for trigger, region, quality in itertools.product(triggers,
                                                      c.DETECTOR_REGIONS,
                                                      c.WORKING_POINTS):
        # Skipping 0eta105 triggers (only defined in Barrel) in the Endcap
        if ("0eta105" in trigger) and (region == "Endcap"):
            continue

        dir_fmt = "ZmumuTPMerged/"+quality+\
            "MuonProbes_"+region.capitalize()+"/OC/"+trigger+"/"
        probe_dir = dir_fmt+"Probe/"
        match_dir = dir_fmt+"Match/"

        hist_fmt_2d = quality+"MuonProbes_"+region.capitalize()+\
            "_OC_"+trigger+"_{}_etaphi_fine_"+region.capitalize()
        probe_hist_2d = hist_fmt_2d.format("Probe")
        match_hist_2d = hist_fmt_2d.format("Match")

        hist_fmt_1d = quality+"MuonProbes_"+region.capitalize()+\
            "_OC_"+trigger+"_{}_"
        probe_fmt_1d = hist_fmt_1d.format("Probe")
        match_fmt_1d = hist_fmt_1d.format("Match")

        probe_eta_hist_1d = probe_fmt_1d+region.capitalize()+"EtaFine"
        probe_phi_hist_1d = probe_fmt_1d+region.capitalize()+"PhiFine"
        probe_pt_hist_1d = probe_fmt_1d+"Pt1D"

        match_eta_hist_1d = match_fmt_1d+region.capitalize()+"EtaFine"
        match_phi_hist_1d = match_fmt_1d+region.capitalize()+"PhiFine"
        match_pt_hist_1d = match_fmt_1d+"Pt1D"

        branches_map = {
            "2D": [
                probe_dir + probe_hist_2d,
                match_dir + match_hist_2d
            ],
            "1D": [
                probe_dir + probe_eta_hist_1d,
                probe_dir + probe_phi_hist_1d,
                probe_dir + probe_pt_hist_1d,
                match_dir + match_eta_hist_1d,
                match_dir + match_phi_hist_1d,
                match_dir + match_pt_hist_1d,
            ]
        }
        for branch_name in branches_map[dim]:
            branch = root_file.Get(branch_name)
            # ensure branches exist
            try:
                branch.GetName()
                # ensure we have a non-zero number of entries
                n_entries = branch.GetEntries()
                if n_entries == 0:
                    exist_but_no_entries.append(branch_name)
            except ReferenceError:
                branches_missing.append(branch_name)
    if len(branches_missing) == 0 and len(exist_but_no_entries) == 0:
        return None
    return branches_missing, exist_but_no_entries

def is_large(wtph_out_file):
    """
    Return whether or not a wtph output file is somewhat large
    (i.e. not the filesize that gets created before the file is filled).
    """

    # 1000 is kind of arbitrary, sometimes it's 420, sometimes 700+
    # but I've never seen a properly-finished WTPH output file with size>1000
    return os.path.getsize(wtph_out_file) > 1000

def get_problems():
    if not exists(c.WTPH_OUTPUT_DIR):
        raise ValueError("WTPH output file dir doesn't exist!")

    file_does_not_exist = []
    branches_dont_exist = []
    histos_with_no_entries = []

    probs = {}  # (problems, not probabilities)

    # For each year, systematic, and period, see if wtph was run
    for year in c.YEARS:
        for prd in get_periods(year, sort=True):
            print(year, prd)
            for syst, trig_type, data_mc, dim in itertools.product(
                 c.VARIATIONS[c.NTUPLE_VERSION],
                 c.TRIGGER_TYPES, ["data", "mc"], ["2D", "1D"]):
                # (single = whether or not these are single-muon triggers)
                single = trig_type == "SingleMuonTriggers"
                fname = data_mc+str(year)+"_"+prd+"_"+syst+\
                    "_"+c.NTUPLE_VERSION+"_"+trig_type+\
                    "_"+dim+".root"
                wtph_file = join(c.WTPH_OUTPUT_DIR, fname)
                # if the file exists already
                # and it is semi-large (i.e. actually finished)
                file_missing = False
                branches_missing = None
                zero_entry_histos = None
                if exists(wtph_file) and is_large(wtph_file):
                    # if not missing any histograms
                    # / has none with 0 entries
                    histo_probs = histo_problems(
                        single, year, prd, wtph_file)
                    if histo_probs is None:
                        # skip this one
                        continue
                    else:
                        branches_missing, zero_entry_histos = histo_probs
                else:
                    file_missing = True

                # If we made it this far, something is wrong with the file
                # (either file is missing or branches don't exist)
                if file_missing and fname not in file_does_not_exist:
                        file_does_not_exist.append(fname)
                        print("\t", fname, "does not exist")
                elif len(branches_missing) != 0:
                    if fname not in branches_dont_exist:
                        branches_dont_exist.append(fname)
                        print("\t", fname, "is missing branch(es)")
                elif len(zero_entry_histos) != 0:
                    if fname not in histos_with_no_entries:
                        histos_with_no_entries.append(fname)
                        print("\t", fname, "has histos with zero entries")
                    for histo in zero_entry_histos:
                        histo_name = histo.split()[-1]
                        hist_type, qual, rgn, trig = parse_histo(histo_name)
                        if year not in probs:
                            probs[year] = {}
                        if prd not in probs[year]:
                            probs[year][prd] = {}
                        if trig not in probs[year][prd]:
                            probs[year][prd][trig] = {}
                            probs[year][prd][trig]["regions"] = []
                            probs[year][prd][trig]["types"] = []
                            probs[year][prd][trig]["qualities"] = []
                            probs[year][prd][trig]["systematics"] = []
                            probs[year][prd][trig]["data/mc"] = []
                            probs[year][prd][trig]["files"] = []
                        if rgn not in probs[year][prd][trig]["regions"]:
                            probs[year][prd][trig]["regions"].append(rgn)
                        if hist_type not in probs[year][prd][trig]["types"]:
                            probs[year][prd][trig]["types"].append(hist_type)
                        if qual not in probs[year][prd][trig]["qualities"]:
                            probs[year][prd][trig]["qualities"].append(qual)
                        if syst not in probs[year][prd][trig]["systematics"]:
                            probs[year][prd][trig]["systematics"].append(syst)
                        if data_mc not in probs[year][prd][trig]["data/mc"]:
                            probs[year][prd][trig]["data/mc"].append(data_mc)
                        if fname not in probs[year][prd][trig]["files"]:
                            probs[year][prd][trig]["files"].append(fname)
                else:
                    raise ValueError(fname)

    make_trigger_table(probs)
    return file_does_not_exist, branches_dont_exist, histos_with_no_entries


def prepare_for_reruns():
    """Check if required WTPH output exists, return text describing errors."""

    # assuming configs/wtph output were generated with these modules,
    # they must already have the right naming scheme
    file_does_not_exist, branches_dont_exist, histos_with_no_entries = \
        get_problems()

    print()
    print("n_files_do_not_exist:", len(file_does_not_exist))
    print("n_branches_do_not_exist:", len(branches_dont_exist))
    print("n_histo_has_no_entries:", len(histos_with_no_entries))

    needs_rerun = sorted(list(set(file_does_not_exist + branches_dont_exist)))

    rerun_lines = []
    line_data = []
    print("Files that need reruns:")
    if len(needs_rerun) == 0:
        print("None.")
    for filename in needs_rerun:
        print(filename)
        dt_mc, year, prd, syst, _, trig_type = \
            parse_wtph_output_filename(filename)
        if [year, prd, syst, trig_type, dt_mc] not in line_data:
            line_data.append([year, prd, syst, trig_type, dt_mc])

            # get the bash scripts that runs WTPH
            job_file = join(
                c.WTPH_JOB_AREA,
                "{}{}-{}-{}-{}.sh".format(dt_mc, year, prd, syst, trig_type))
            assert exists(job_file)

            # get ATLAS-formatted files as required for Cedar
            atlfile = join(
                c.WTPH_JOB_AREA,
                "atlas-{}{}-{}-{}-{}.sh".format(
                    dt_mc, year, prd, syst, trig_type))

            output_file = join(
                c.WTPH_SUBMIT_AREA,
                "{}{}{}-{}-{}-{}.txt".format(
                    c.NTUPLE_VERSION, dt_mc, year, prd, syst, trig_type))

            rerun_lines += [
                # batchScript = command to make the atlas-file
                'batchScript "source ' + job_file +\
                '" -O ' + atlfile,
                "echo 'Submitting " + atlfile + "'",
                "cd " + c.WTPH_SUBMIT_AREA,
                "sbatch" +\
                " --account=" + c.ACCOUNT +\
                " --ntasks=1" +\
                " --cpus-per-task=4" +\
                " --mem=" + c.DOUBLE_MEM +\
                " --time=" + c.EXTRA_TIME +\
                " --mail-user=" + c.MAIL_USER +\
                " --mail-type=" + c.MAIL_TYPE +\
                " --output=" + output_file +\
                " "+ atlfile,
                "cd " + os.getcwd()]

    with open(RERUN_FILE, "w") as subfile:
        subfile.write("\n".join(rerun_lines))

    print(len(needs_rerun), "jobs remaining to run")
    print("To run all jobs: source " + RERUN_FILE)



if __name__ == "__main__":
    prepare_for_reruns()
