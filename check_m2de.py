"""Module to check if make_2d_eff finished successfully."""
from os.path import exists, join

from ROOT import TFile

import core.constants as c
from core.run_numbers import get_periods
from core.triggers import triggers_in_period

DIR_FMT = "{qual}/Period{prd}/{trig}/"
HISTO_FMT = "sf_{rgn}_{var}"

for year in c.YEARS:
    syear = str(year)[2:]  # e.g. "15" from 2015

    # check that SFPlots_yy_version.root exists
    plots_filepath = join(c.M2DE_OUTPUT_DIR,
                    "SFPlots_" + syear + "_" + c.NTUPLE_VERSION + ".root")
    if not exists(plots_filepath):
        raise FileNotFoundError("File not found: " + plots_filepath)

    # open the plots file
    plots_file = TFile(plots_filepath)

    # check that all required branches exist
    for quality in c.WORKING_POINTS:
        for period in get_periods(year):
            for single in [True, False]:
                for trigger in triggers_in_period(single, year, period):
                    dir_name = DIR_FMT.format(
                        qual=quality, prd=period, trig=trigger)
                    for region in c.DETECTOR_REGIONS:
                        for variation in c.VARIATIONS[c.NTUPLE_VERSION]:
                            histo_name = HISTO_FMT.format(
                                rgn=region.lower(), var=variation)
                            histo_path = join(dir_name, histo_name)

                            # ensure histo exists
                            histo = plots_file.Get(histo_path)
                            try:
                                histo.GetName()
                            except ReferenceError as exc:
                                print(plots_filepath, histo_path)
                                raise ReferenceError("Histo DNE") from exc

                            # ensure it has more than 0 entries
                            n_entries = histo.GetEntries()
                            if n_entries == 0:
                                print(plots_filepath, histo_path)
                                raise ValueError("No entries in histo!")






