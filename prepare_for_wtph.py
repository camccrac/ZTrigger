"""
Run WriteTagProbeHistos, the nice way :)
"""
from __future__ import print_function
import getpass
import os
import core.constants as c
from core.run_numbers import RUN_NUMBERS
from config_makers.wtph_input_configs import format_input_configs

NTUPV = c.NTUPLE_VERSION  # to make lines shorter later

# ensure that input configs have the propper naming scheme
print("formatting input configs")
format_input_configs()

# assuming run configs were generated with these modules,
# they must already have the right naming scheme

# First, some paths and such:
if not os.path.exists(c.WTPH_OUTPUT_DIR):
    os.mkdir(c.WTPH_OUTPUT_DIR)
if not os.path.exists(c.WTPH_JOB_AREA):
    os.mkdir(c.WTPH_JOB_AREA)

INPUT_CONFIGS = os.listdir(c.WTPH_INPUT_CONF_DIR)

# job parameters like time limit and slurm account are set in constants module

# name of bash script that will submit all batch jobs
SUBMIT_FILE = os.path.join(c.WTPH_JOB_AREA, "submitAllWTPH.sh")
if os.path.exists(SUBMIT_FILE):
    os.remove(SUBMIT_FILE)

PW = getpass.getpass("Please enter your grid password:")
CWD = os.getcwd()

# setup stuff for individual scripts
HEADER = "#!/bin/bash\n"
SETUP = 'lsetup "rucio SL7Python2"\n' +\
    "echo " + PW + " | voms-proxy-init -voms atlas\n" +\
    "cd " + os.path.join(c.MTPPP_ROOT, "build") + "\n" +\
    "asetup\n" +\
    "source */setup.sh\n" +\
    "mkdir -p " + c.WTPH_OUTPUT_DIR + "\n" +\
    "cd " + os.path.join(c.MTPPP_ROOT, "run") + "\n"

# stuff we'll write into the master script to run at the end
MAIN_FILE_LINES = []

# For each year, systematic, and period, make a script to run WTPH
print("making WTPH batch scripts")
for year in c.YEARS:
    for prd in RUN_NUMBERS[year]:
        print(year, prd)
        for syst in c.VARIATIONS[c.NTUPLE_VERSION]:
            for trigs in c.TRIGGER_TYPES:
                for data_mc in ["data", "mc"]:

                    # find run configs
                    run_configs = os.listdir(
                        os.path.join(c.WTPH_RUN_CONF_DIR, trigs))
                    run_config_name = "MuonProbes_" + trigs + \
                        "_{syst}_{year}_{prd}_{data_mc}.conf".format(
                            syst=syst, year=year, prd=prd, data_mc=data_mc)
                    assert run_config_name in run_configs
                    run_conf_dir = os.path.join(c.WTPH_RUN_CONF_DIR, trigs)
                    run_config = os.path.join(run_conf_dir, run_config_name)

                    # find input configs, guaranteed to exist from earlier code
                    input_file = os.path.join(
                        c.WTPH_INPUT_CONF_DIR,
                        c.WTPH_INPUT_CONF_FMT[data_mc].format(
                            year=year, period=prd))

                    # output files we'll write to
                    output_file_2d = os.path.join(
                        c.WTPH_OUTPUT_DIR,
                        data_mc + str(year) + "_" + prd + "_" +\
                        syst + "_" + NTUPV + "_" + trigs + "_2D.root")
                    output_file_1d = os.path.join(
                        c.WTPH_OUTPUT_DIR,
                        data_mc + str(year) + "_" + prd + "_" +\
                        syst + "_" + NTUPV + "_" + trigs + "_1D.root")

                    # write the bash scripts that runs WTPH
                    job_filename = "{dmc}{yr}-{prd}-{syst}-{trigs}.sh".format(
                        dmc=data_mc, yr=year, prd=prd, syst=syst, trigs=trigs)
                    job_file = os.path.join(
                        c.WTPH_JOB_AREA, job_filename)

                    wtph_command_2d = "WriteTagProbeHistos"\
                        " -i "+ input_file +\
                        " -h " + c.WTPH_2D_HISTO_CONF +\
                        " -r " + run_config +\
                        " -o " + output_file_2d + "\n"
                    wtph_command_1d = "WriteTagProbeHistos"\
                        " -i "+ input_file +\
                        " -h " + c.WTPH_1D_HISTO_CONF +\
                        " -r " + run_config +\
                        " -o " + output_file_1d + "\n"

                    with open(job_file, "w") as jfile:
                        jfile.write(
                            HEADER + SETUP + wtph_command_2d + wtph_command_1d)

                    # make the ATLAS-formatted job files as required for Cedar
                    atlfile = os.path.join(
                        c.WTPH_JOB_AREA, "atlas-"+job_filename)

                    output_file = os.path.join(
                        c.WTPH_SUBMIT_AREA,
                        "{v}{data_mc}{year}-{prd}-{syst}-{trigs}.txt".format(
                            v=c.NTUPLE_VERSION, data_mc=data_mc, year=year,
                            prd=prd, syst=syst, trigs=trigs))

                    MAIN_FILE_LINES += [
                        # batchScript = command to make the atlas-format file
                        'batchScript "source ' + job_file + '" -O '+ atlfile,
                        "echo 'Submitting " + atlfile + "'",
                        "cd " + c.WTPH_SUBMIT_AREA,
                        "sbatch" +\
                        " --account=" + c.ACCOUNT +\
                        " --ntasks=1" +\
                        " --cpus-per-task=4" +\
                        " --mem=" + c.MEM +\
                        " --time=" + c.TIME +\
                        " --mail-user=" + c.MAIL_USER +\
                        " --mail-type=" + c.MAIL_TYPE +\
                        " --output=" + output_file +\
                        " "+ atlfile,
                        "cd " + CWD]

with open(SUBMIT_FILE, "w") as subfile:
    subfile.write("\n".join(MAIN_FILE_LINES))

print("To run all jobs: source " + SUBMIT_FILE)
