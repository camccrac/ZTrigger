"""
Module for storing constants,
like the list of variation names and template file paths.
"""
from __future__ import print_function
import os

NTUPLE_VERSION = os.getenv("ntupleVersion")
if NTUPLE_VERSION is None:
    print("ntupleVersion environment variable not set, defaulting to v66.3.0")
    NTUPLE_VERSION = "v66.3.0"
else:
    print("Using NTuple version:", NTUPLE_VERSION)
USER = os.getenv("USER")

# stuff for slurm
TIME = "8:00:00"  # time limit for batch jobs
EXTRA_TIME = "20:00:00"  # time limit for re-run jobs
MEM = "4GB"
DOUBLE_MEM = "8GB"  # double for re-run jobs
ACCOUNT = "ctb-stelzer"  # "def-osc-ab"  # slurm account
RELEASE = "AthAnalysis,21.2.196"  # ATLAS software release you set up
MAIL_USER = "callum.mccracken@cern.ch"  # what to use for job-related emails
MAIL_TYPE = "FAIL"  # how many emails do you want? BEGIN END FAIL REQUEUE ALL

# stuff for looping over
YEARS = [2015, 2016, 2017, 2018]
SYEARS = [str(year)[2:] for year in YEARS]  # short string years like "15"
VARIATIONS = {
    "v66.3.0": [
        "nominal", "dphill", "mll",
        "muneg", "mupos", "noIP", "nvtx_dw", "nvtx_up", "ptdw", "ptup",
        "isoPflowLoose_VarRad", "isoPflowTight_VarRad",
        "isoLoose_VarRad", "isoTight_VarRad"
        ],
    "v65.3.2": [
        "nominal", "dphill", "mll",
        "muneg", "mupos", "noIP", "nvtx_dw", "nvtx_up", "ptdw", "ptup",
        "isoTight", "isoTightTrackOnly"],
    "v064": [
        "nominal", "dphill", "mll",
        "muneg", "mupos", "noIP", "nvtx_dw", "nvtx_up", "ptdw", "ptup",
        "isoTight", "isoTightTrackOnly"],
    }
WORKING_POINTS = ["Medium", "Loose", "Tight", "HighPt"]

# stuff for MC file naming conventions
RELEASE_FROM_NTUPLE_VERSION = {
    "v66.3.0": 22,
    "v65.3.2": 21,
    # "v064": 21,
}
NTUPLE_VERSION_FROM_RELEASE = {
    release: ntuple_version
    for ntuple_version, release in RELEASE_FROM_NTUPLE_VERSION.items()}
RELEASE = RELEASE_FROM_NTUPLE_VERSION[NTUPLE_VERSION]
MC_NUMBER_MAP = {
    22: "20",
    21: "16"
}
MC_NUMBER = MC_NUMBER_MAP[RELEASE]
MC_CAMPAIGN_LETTERS = {
    "15": "a",
    "16": "a",
    "17": "d",
    "18": "e",
}
MC_CAMPAIGNS = {year: MC_NUMBER+MC_CAMPAIGN_LETTERS[year] for year in SYEARS}
R_TAGS = {  # need ones that correspond to the values in MC_CAMPAIGNS
    "16a": "r9364",  # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC16
    "16d": "r10201",
    "16e": "r10724",
    "20a": "r13167",  # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasProductionGroupMC20
    "20d": "r13144",
    "20e": "r13145",
}


# Useful paths
CORE_DIR = os.path.dirname(__file__)
ZTRIGGER_DIR = os.path.realpath(os.path.join(CORE_DIR, "../"))
MTPPP_ROOT = os.path.realpath(os.path.join(ZTRIGGER_DIR, "../../../../../"))
MTPPP_DATA_PATH = os.path.join(
    MTPPP_ROOT, "MuonTPPostProcessing/MuonTPPostProcessing/data")
# where to store output from WTPH
WTPH_OUTPUT_DIR = os.path.join(MTPPP_ROOT, "output")
# where wtph job scripts will be written
WTPH_JOB_AREA = os.path.join(WTPH_OUTPUT_DIR, "WTPHJobFiles/")
# where you submit your batch jobs from (cannot be in /home/)
WTPH_SUBMIT_AREA = os.path.join("/scratch/", USER)
# where the input conigs are read from
WTPH_INPUT_CONF_DIR = os.path.join(
    MTPPP_DATA_PATH, "InputConfTRIUMF_"+NTUPLE_VERSION)
# which histo config to use
WTPH_2D_HISTO_CONF = os.path.join(
    MTPPP_DATA_PATH, "HistoConf/ZTrigger/SingleMuonTriggers/2D_hist.conf")
WTPH_1D_HISTO_CONF = os.path.join(
    MTPPP_DATA_PATH, "HistoConf/ZTrigger/SingleMuonTriggers/1D_hist.conf")
# where the run configs are stored
WTPH_RUN_CONF_DIR = os.path.join(
    MTPPP_DATA_PATH, "RunConf/ZTrigger/")
# how the input configs should be named by the time WTPH scripts are written
WTPH_INPUT_CONF_FMT = {
    "data": "data_{year}_{period}.conf",
    "mc": "{year}_{period}_Zmumu.conf"}

# where to store scale factors and such
RUN_DIR = os.path.join(MTPPP_ROOT, "run")

# where we are now, relative to MuonTPPostProcessing

# directory for storing single muon stuff, relative to TOP_LEVEL_DIR
SINGLE_MUON_DIR = os.path.join(ZTRIGGER_DIR, "SingleMuonTriggers")
MULTI_LEG_DIR = os.path.join(ZTRIGGER_DIR, "MultiLegTriggers")
TRIGGER_TYPES = ["SingleMuonTriggers", "MultiLegTriggers"]

# template files -- SM = Single Muon, ML = Multi Leg
DETECTOR_REGION_TEMPLATE = os.path.join(
    ZTRIGGER_DIR, "templates/DetectorRegionsForZmumuReco.conf")
BASIC_CONFIG_TEMPLATE = os.path.join(
    ZTRIGGER_DIR, "templates/BasicConfigZMuon.conf")
SM_NOMINAL_CONFIG_TEMPLATE = os.path.join(
    ZTRIGGER_DIR, "templates/SM_nominal_template_{data_mc}.conf")
PRE_NOMINAL_CONFIG_TEMPLATE = os.path.join(
    ZTRIGGER_DIR, "templates/pre_nominal_template.conf")
MATCHES_TEMPLATE = os.path.join(
    ZTRIGGER_DIR, "templates/MatchesForZmumuMuon.conf")
ML_NOMINAL_CONFIG_TEMPLATE = os.path.join(
    ZTRIGGER_DIR, "templates/ML_nominal_template_{data_mc}.conf")

# spots to store output files, relative to top
DETECTOR_REGIONS_SAVE_PATH = os.path.join(
    SINGLE_MUON_DIR, os.path.basename(DETECTOR_REGION_TEMPLATE))
SM_BASIC_CONFIG_PATH_FMT = os.path.join(
    SINGLE_MUON_DIR, "BasicConfigZMuon_{year}_{period}.conf")
SM_MATCHES_CONFIG_PATH_FMT = os.path.join(
    SINGLE_MUON_DIR, "MatchesForZmumuMuon_{year}_{period}.conf")
SM_VAR_CONFIG_PATH_FMT = os.path.join(
    SINGLE_MUON_DIR,
    "MuonProbes_SingleMuonTriggers_{variation}_{year}_{period}_{data_mc}.conf")

ML_BASIC_CONFIG_PATH_FMT = os.path.join(
    MULTI_LEG_DIR, "BasicConfigZMuon_{year}_{period}.conf")
ML_MATCHES_CONFIG_PATH_FMT = os.path.join(
    MULTI_LEG_DIR, "MatchesForZmumuMuon_{year}_{period}.conf")
ML_VAR_CONFIG_PATH_FMT = os.path.join(
    MULTI_LEG_DIR,
    "MuonProbes_MultiLegTriggers_{variation}_{year}_{period}_{data_mc}.conf")

def get_detector_regions():
    """Get detector regions from detector region template file."""
    with open(os.path.join(ZTRIGGER_DIR,
                           DETECTOR_REGION_TEMPLATE), "r") as det_file:
        lines = det_file.readlines()
        name_lines = [l for l in lines if "RegionName" in l]
        names = [nl.split()[1] for nl in name_lines]
    return names

DETECTOR_REGIONS = get_detector_regions()

# make_2d_eff SF output file naming convention

M2DE_INPUT_DIR = WTPH_OUTPUT_DIR
M2DE_OUTPUT_DIR = RUN_DIR
