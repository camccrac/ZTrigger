SUPPORTED_TRIGGERS = {
    # triggers that are listed as supported on twiki:
    # https://twiki.cern.ch/twiki/bin/viewauth/Atlas/MuonTriggerPhysicsRecommendationsRel212017
    2015: {
        "D": [  # D-J
            # single
            "HLT_mu20_iloose_L1MU15",
            "HLT_mu26_imedium",
            "HLT_mu40",
            "HLT_mu50",
            "HLT_mu20_iloose_L1MU15_OR_HLT_mu40",
            "HLT_mu20_iloose_L1MU15_OR_HLT_mu50",
            "HLT_mu26_imedium_OR_HLT_mu40",
            "HLT_mu26_imedium_OR_HLT_mu50",
            # multi
            "HLT_mu10",
            "HLT_mu14",
            "HLT_mu18",
            "HLT_mu22",
            "HLT_mu24",
            "HLT_mu8noL1"
        ]
    },
    2016: {
        "A": [
            # single
            "HLT_mu24_iloose_OR_HLT_mu24_iloose_L1MU15",
            "HLT_mu24_iloose_OR_HLT_mu24_iloose_L1MU15_OR_HLT_mu40",
            "HLT_mu24_iloose_OR_HLT_mu24_iloose_L1MU15_OR_HLT_mu50",
            "HLT_mu24_imedium",
            "HLT_mu24_imedium_OR_HLT_mu40",
            "HLT_mu24_imedium_OR_HLT_mu50",
            "HLT_mu24_ivarmedium",
            "HLT_mu24_ivarmedium_OR_HLT_mu40",
            "HLT_mu24_ivarmedium_OR_HLT_mu50",
            "HLT_mu26_imedium",
            "HLT_mu26_imedium_OR_HLT_mu40",
            "HLT_mu26_imedium_OR_HLT_mu50",
            "HLT_mu26_ivarmedium",
            "HLT_mu26_ivarmedium_OR_HLT_mu40",
            "HLT_mu26_ivarmedium_OR_HLT_mu50",
            "HLT_mu40",
            "HLT_mu50",
            # multi
            "HLT_mu10",
            "HLT_mu14",
            "HLT_mu14_ivarloose",
            "HLT_mu18",
            "HLT_mu20",
            "HLT_mu22",
            "HLT_mu24",
            "HLT_mu26",
            "HLT_mu8noL1" 
        ],
        "B": [  # B-D3
            # single
            "HLT_mu24_imedium",
            "HLT_mu24_imedium_OR_HLT_mu50",
            "HLT_mu24_ivarmedium",
            "HLT_mu24_ivarmedium_OR_HLT_mu50",
            "HLT_mu26_imedium",
            "HLT_mu26_imedium_OR_HLT_mu50",
            "HLT_mu26_ivarmedium",
            "HLT_mu26_ivarmedium_OR_HLT_mu50",
            "HLT_mu50",
            # multi
            "HLT_mu10",
            "HLT_mu14",
            "HLT_mu14_ivarloose",
            "HLT_mu18",
            "HLT_mu20",
            "HLT_mu22",
            "HLT_mu24",
            "HLT_mu26",
            "HLT_mu8noL1" 
        ],
        "D4D8": [  # D4-F
            # single
            "HLT_mu26_imedium",
            "HLT_mu26_imedium_OR_HLT_mu50",
            "HLT_mu26_ivarmedium",
            "HLT_mu26_ivarmedium_OR_HLT_mu50",
            "HLT_mu50",
            # multi
            "HLT_mu10",
            "HLT_mu14",
            "HLT_mu14_ivarloose",
            "HLT_mu18",
            "HLT_mu22",
            "HLT_mu24",
            "HLT_mu26",
            "HLT_mu8noL1"
        ],
        "G": [  # G-L
            # single
            "HLT_mu26_ivarmedium",
            "HLT_mu26_ivarmedium_OR_HLT_mu50",
            "HLT_mu50",
            # multi
            "HLT_mu10",
            "HLT_mu14",
            "HLT_mu14_ivarloose",
            "HLT_mu18",
            "HLT_mu22",
            "HLT_mu24",
            "HLT_mu26",
            "HLT_mu8noL1"
        ]
    },
    2017: {
        "B": [  # B-K
            # single
            "HLT_mu26_ivarmedium",
            "HLT_mu26_ivarmedium_OR_HLT_mu50",
            "HLT_mu50",
            # multi
            "HLT_mu14",
            "HLT_mu14_ivarloose",
            "HLT_mu20",
            "HLT_mu22",
            "HLT_mu24",
            "HLT_mu8noL1" 
        ]
    },
    2018: {
        "B": [  # B-M
            # single
            "HLT_mu26_ivarmedium",
            "HLT_mu26_ivarmedium_OR_HLT_mu50",
            "HLT_mu50",
            # multi
            "HLT_mu10",
            "HLT_mu14",
            "HLT_mu14_ivarloose",
            "HLT_mu20",
            "HLT_mu22",
            "HLT_mu24",
            "HLT_mu8noL1" 
        ]
    }
}
# triggers are the same for some periods
SUPPORTED_TRIGGERS[2015]["E"] = SUPPORTED_TRIGGERS[2015]["D"]
SUPPORTED_TRIGGERS[2015]["F"] = SUPPORTED_TRIGGERS[2015]["D"]
SUPPORTED_TRIGGERS[2015]["G"] = SUPPORTED_TRIGGERS[2015]["D"]
SUPPORTED_TRIGGERS[2015]["H"] = SUPPORTED_TRIGGERS[2015]["D"]
SUPPORTED_TRIGGERS[2015]["I"] = SUPPORTED_TRIGGERS[2015]["D"]
SUPPORTED_TRIGGERS[2015]["J"] = SUPPORTED_TRIGGERS[2015]["D"]

SUPPORTED_TRIGGERS[2016]["C"] = SUPPORTED_TRIGGERS[2016]["B"]
SUPPORTED_TRIGGERS[2016]["D1D3"] = SUPPORTED_TRIGGERS[2016]["B"]

SUPPORTED_TRIGGERS[2016]["E"] = SUPPORTED_TRIGGERS[2016]["D4D8"]
SUPPORTED_TRIGGERS[2016]["F"] = SUPPORTED_TRIGGERS[2016]["D4D8"]

SUPPORTED_TRIGGERS[2016]["H"] = SUPPORTED_TRIGGERS[2016]["G"]
SUPPORTED_TRIGGERS[2016]["I"] = SUPPORTED_TRIGGERS[2016]["G"]
SUPPORTED_TRIGGERS[2016]["J"] = SUPPORTED_TRIGGERS[2016]["G"]
SUPPORTED_TRIGGERS[2016]["K"] = SUPPORTED_TRIGGERS[2016]["G"]
SUPPORTED_TRIGGERS[2016]["L"] = SUPPORTED_TRIGGERS[2016]["G"]

SUPPORTED_TRIGGERS[2016]["H"] = SUPPORTED_TRIGGERS[2016]["G"]
SUPPORTED_TRIGGERS[2016]["I"] = SUPPORTED_TRIGGERS[2016]["G"]
SUPPORTED_TRIGGERS[2016]["J"] = SUPPORTED_TRIGGERS[2016]["G"]
SUPPORTED_TRIGGERS[2016]["K"] = SUPPORTED_TRIGGERS[2016]["G"]
SUPPORTED_TRIGGERS[2016]["L"] = SUPPORTED_TRIGGERS[2016]["G"]

SUPPORTED_TRIGGERS[2017]["C"] = SUPPORTED_TRIGGERS[2017]["B"]
SUPPORTED_TRIGGERS[2017]["D"] = SUPPORTED_TRIGGERS[2017]["B"]
SUPPORTED_TRIGGERS[2017]["E"] = SUPPORTED_TRIGGERS[2017]["B"]
SUPPORTED_TRIGGERS[2017]["F"] = SUPPORTED_TRIGGERS[2017]["B"]
SUPPORTED_TRIGGERS[2017]["G"] = SUPPORTED_TRIGGERS[2017]["B"]
SUPPORTED_TRIGGERS[2017]["H"] = SUPPORTED_TRIGGERS[2017]["B"]
SUPPORTED_TRIGGERS[2017]["I"] = SUPPORTED_TRIGGERS[2017]["B"]
SUPPORTED_TRIGGERS[2017]["J"] = SUPPORTED_TRIGGERS[2017]["B"]
SUPPORTED_TRIGGERS[2017]["K"] = SUPPORTED_TRIGGERS[2017]["B"]

SUPPORTED_TRIGGERS[2018]["C"] = SUPPORTED_TRIGGERS[2018]["B"]
SUPPORTED_TRIGGERS[2018]["D"] = SUPPORTED_TRIGGERS[2018]["B"]
SUPPORTED_TRIGGERS[2018]["E"] = SUPPORTED_TRIGGERS[2018]["B"]
SUPPORTED_TRIGGERS[2018]["F"] = SUPPORTED_TRIGGERS[2018]["B"]
SUPPORTED_TRIGGERS[2018]["G"] = SUPPORTED_TRIGGERS[2018]["B"]
SUPPORTED_TRIGGERS[2018]["H"] = SUPPORTED_TRIGGERS[2018]["B"]
SUPPORTED_TRIGGERS[2018]["I"] = SUPPORTED_TRIGGERS[2018]["B"]
SUPPORTED_TRIGGERS[2018]["J"] = SUPPORTED_TRIGGERS[2018]["B"]
SUPPORTED_TRIGGERS[2018]["K"] = SUPPORTED_TRIGGERS[2018]["B"]
SUPPORTED_TRIGGERS[2018]["L"] = SUPPORTED_TRIGGERS[2018]["B"]
SUPPORTED_TRIGGERS[2018]["M"] = SUPPORTED_TRIGGERS[2018]["B"]

def is_supported(year, period, trigger_name):
    """Return whether a given trigger is supported (in a given year/period)."""
    return trigger_name in SUPPORTED_TRIGGERS[year][period]
