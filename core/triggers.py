"""Module storing info about which triggers to use in which periods."""

TRIGGERS = {  # TRIGGERS[year][period][SINGLE/MULTI] = [list of triggers]
    2015: {
        "ANY": {
            "SINGLE": [
                "HLT_mu20_iloose_L1MU15",
                "HLT_mu40",
                #"HLT_mu60_0eta105_msonly",
                "HLT_mu20_iloose_L1MU15_OR_HLT_mu40",
                #"HLT_mu20_iloose_L1MU15_OR_HLT_mu40_" +\
                #"OR_HLT_mu60_0eta105_msonly",
            ],
            "MULTI": [
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1"
                "HLT_mu10",
                "HLT_mu14",
                "HLT_mu18",
                "HLT_mu22",
                "HLT_mu24",
            ]
        }
    },
    2016: {
        "A": {
            "SINGLE": [
                "HLT_mu24_iloose",
                "HLT_mu24_ivarloose",
                "HLT_mu24_imedium",
                "HLT_mu24_ivarmedium",
                "HLT_mu26_imedium",
                "HLT_mu26_ivarmedium",
                "HLT_mu40",
                "HLT_mu50",
                # Each mu24 trigger in OR with mu40
                "HLT_mu24_iloose_OR_HLT_mu40",
                "HLT_mu24_ivarloose_OR_HLT_mu40",
                "HLT_mu24_imedium_OR_HLT_mu40",
                "HLT_mu24_ivarmedium_OR_HLT_mu40",
                # Each mu24 trigger in OR with mu50
                "HLT_mu24_iloose_OR_HLT_mu50",
                "HLT_mu24_ivarloose_OR_HLT_mu50",
                "HLT_mu24_imedium_OR_HLT_mu50",
                "HLT_mu24_ivarmedium_OR_HLT_mu50",
                # Each mu26 trigger in OR with mu40
                "HLT_mu26_imedium_OR_HLT_mu40",
                "HLT_mu26_ivarmedium_OR_HLT_mu40",
                # Each mu26 trigger in OR with mu50
                "HLT_mu26_imedium_OR_HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                # "HLT_mu6_msonly", Missing in branches
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        },
        "B": {
            "SINGLE": [
                "HLT_mu24_imedium",
                "HLT_mu24_ivarmedium",
                "HLT_mu26_imedium",
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                # Each mu24 trigger in OR with mu50
                "HLT_mu24_imedium_OR_HLT_mu50",
                "HLT_mu24_ivarmedium_OR_HLT_mu50",
                # Each mu26 trigger in OR with mu50
                "HLT_mu26_imedium_OR_HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        },
        "C": {
            "SINGLE": [
                "HLT_mu24_imedium",
                "HLT_mu24_ivarmedium",
                "HLT_mu26_imedium",
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                #Each mu24 trigger in OR with mu50
                "HLT_mu24_imedium_OR_HLT_mu50",
                "HLT_mu24_ivarmedium_OR_HLT_mu50",
                #Each mu26 trigger in OR with mu50
                "HLT_mu26_imedium_OR_HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        },
        "D1D3": {
            "SINGLE": [
                "HLT_mu24_imedium",
                "HLT_mu24_ivarmedium",
                "HLT_mu26_imedium",
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                #Each mu24 trigger in OR with mu50
                "HLT_mu24_imedium_OR_HLT_mu50",
                "HLT_mu24_ivarmedium_OR_HLT_mu50",
                #Each mu26 trigger in OR with mu50
                "HLT_mu26_imedium_OR_HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        },
        "D4D8": {
            "SINGLE": [
                "HLT_mu26_imedium",
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                #Each mu26 trigger in OR with mu50
                "HLT_mu26_imedium_OR_HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        },
        "E": {
            "SINGLE": [
                "HLT_mu26_imedium",
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                #Each mu26 trigger in OR with mu50
                "HLT_mu26_imedium_OR_HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        },
        "F": {
            "SINGLE": [
                "HLT_mu26_imedium",
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                # Each mu26 trigger in OR with mu50
                "HLT_mu26_imedium_OR_HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        },
        "G": {
            "SINGLE": [
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        },
        "I": {
            "SINGLE": [
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        },
        "K": {
            "SINGLE": [
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        },
        "L": {
            "SINGLE": [
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu10_msonly",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15noL1",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu20_msonly",
                "HLT_mu22",
                "HLT_mu24",
                # "HLT_mu26"
            ]
        }
    },
    2017: {
        "ANY": {
            "SINGLE": [
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                # "HLT_mu60_0eta105_msonly",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
                # "HLT_mu26_ivarmedium_OR_HLT_mu50_OR_HLT_mu60_0eta105_msonly",
            ],
            "MULTI": [
                "HLT_mu4",
                "HLT_mu6",
                "HLT_mu6_msonly",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15",
                "HLT_mu15noL1",
                "HLT_mu18",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu22",
                "HLT_mu24",
            ]
        }
    },
    2018: {
        "ANY": {
            "SINGLE": [
                "HLT_mu26_ivarmedium",
                "HLT_mu50",
                # "HLT_mu60_0eta105_msonly",
                "HLT_mu26_ivarmedium_OR_HLT_mu50",
                # "HLT_mu26_ivarmedium_OR_HLT_mu50_OR_HLT_mu60_0eta105_msonly",
            ],
            "MULTI": [
                "HLT_mu6",
                "HLT_mu8noL1",
                "HLT_mu10",
                "HLT_mu14",
                "HLT_mu14_ivarloose",
                "HLT_mu15",
                "HLT_mu15noL1",
                "HLT_mu18",
                "HLT_mu18noL1",
                "HLT_mu20",
                "HLT_mu22",
                "HLT_mu24",
            ]
        }
    }
}


def triggers_in_period(single, year, period):
    """
    Return the list of triggers in a given period.

    single: bool, True for SingleMuonTriggers, False for MultiLegTriggers
    year: int, 4-digit, like 2015
    period: str, like "B". See run_numbers.py for allowed values
    """
    assert isinstance(single, bool)
    assert isinstance(year, int)
    triggers_in_year = TRIGGERS[year]
    if period in triggers_in_year.keys():
        trigs_in_period = triggers_in_year[period]
    elif "ANY" in triggers_in_year.keys():
        trigs_in_period = triggers_in_year["ANY"]
    else:
        raise KeyError(period)
    trigs = trigs_in_period["SINGLE" if single else "MULTI"]
    return trigs


# Stuff for MatchSelection blocks in matches config files.
# See matches_config.py for how this is used.

SINGLE_TRIGGER_TEMPLATE = """
New_MatchSelection
    MatchName {trigger_name}
    Cut bool probe_matched_{trigger_name} = 1
End_MatchSelection
"""


def single_match_selection(trigger_name):
    """Return MatchSelection block for a single trigger (not an OR one)."""
    return SINGLE_TRIGGER_TEMPLATE.format(trigger_name=trigger_name)


OR_TRIGGER_TEMPLATE = """
New_MatchSelection
    MatchName {trigger_name}
    MatchCombCut OR
        {cut_lines}
    End_CombCut
End_MatchSelection
"""


CUT_LINE = "Cut bool probe_matched_{trigger_name} = 1\n        "


def or_match_selection(trigger_name):
    """Return MatchSelection block for an OR trigger."""
    assert "_OR_" in trigger_name
    trigger_names = trigger_name.split("_OR_")
    cut_lines = [CUT_LINE.format(trigger_name=tnm) for tnm in trigger_names]
    return OR_TRIGGER_TEMPLATE.format(
        trigger_name=trigger_name, cut_lines=cut_lines)

def get_matches_text(single, year, period):
    """single = True if you want SINGLE triggers, else MULTI triggers"""
    trigs = triggers_in_period(single, year, period)
    matches_text = ""
    for trigger in trigs:
        if "_OR_" in trigger:
            matches_text += or_match_selection(trigger)
        else:
            matches_text += single_match_selection(trigger)
    return matches_text
