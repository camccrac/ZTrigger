"""
Module for doing the R21 vs R22 comparison.

(Currently under construction)
"""
from os.path import join, exists
from ROOT import TFile
from ROOT import TObject

from core.run_numbers import get_periods
from core.triggers import triggers_in_period
import core.constants as c

# which releases to compare
RELEASE_1 = 21
RELEASE_2 = 22

# which specific ntuple versions to use (should be set in constants module)
NTUPLE_VERSION_1 = c.NTUPLE_VERSION_FROM_RELEASE[RELEASE_1]
NTUPLE_VERSION_2 = c.NTUPLE_VERSION_FROM_RELEASE[RELEASE_2]

# are there any variations we should ignore? E.g. between R21 and R22 isolation systs were different
VARIATIONS_TO_IGNORE = [
    "isoTight",
    "isoTightTrackOnly",
    "isoTight_VarRad",
    "isoLoose_VarRad",
    "isoPflowTight_VarRad",
    "isoPflowLoose_VarRad",
]

# how histograms are stored
DIR_FMT = "{qual}/Period{prd}/{trig}/"
HISTO_FMT = "sf_{rgn}_{var}"

VARIATIONS = c.VARIATIONS[NTUPLE_VERSION_1]
print("using variations for ntuple version", NTUPLE_VERSION_1,
        "if that's not ok you should specify which variations to use!")
print("ignoring these: " + ",".join(VARIATIONS_TO_IGNORE))

COMP_HISTO_TITLE = "Release "+str(RELEASE_1)+" / Release "+str(RELEASE_2)

def compare(filename_1, filename_2, dir_name):
    """Given two files and a branch name, compare histograms at that branch."""
    if not exists(filename_1):
        raise FileNotFoundError(filename_1)
    if not exists(filename_2):
        raise FileNotFoundError(filename_2)

    variations = [var for var in VARIATIONS if var not in VARIATIONS_TO_IGNORE]

    # open the files
    root_file_1 = TFile(filename_1)
    root_file_2 = TFile(filename_2)

    # check the files have what we need
    for region in c.DETECTOR_REGIONS:
        for variation in variations:
            histo_name = HISTO_FMT.format(rgn=region.lower(), var=variation)
            histo_path = join(dir_name, histo_name)
            # get the histograms
            histo_1 = root_file_1.Get(histo_path)
            histo_2 = root_file_2.Get(histo_path)

            # ensure they actually exist
            for histo in [histo_1, histo_2]:
                # raises ReferenceError if it doesn't exist
                try:
                    histo.GetName()
                except ReferenceError as exc:
                    print(filename_1)
                    print(filename_2)
                    print(histo_path)
                    print(histo)
                    raise ReferenceError(histo_name+" doesn't exist!") from exc

                # ensure we have a non-zero number of entries
                n_entries = histo.GetEntries()
                if n_entries == 0:
                    raise ValueError("Histogram is empty!")

    # now do the actual comparison bit
    comp_filename = "SFPlots_Comparison.root"
    comp_filepath = join(c.M2DE_OUTPUT_DIR, comp_filename)

    print("Writing SF Comparisons to", comp_filepath, ":", dir_name)
    comp_file = TFile(comp_filepath, 'update')
    comp_file.mkdir(dir_name)
    comp_file.cd(dir_name)

    for region in c.DETECTOR_REGIONS:
        for variation in sorted(variations):
            # get histograms as above
            histo_name = HISTO_FMT.format(rgn=region.lower(), var=variation)
            histo_path = join(dir_name, histo_name)
            histo_1 = root_file_1.Get(histo_path)
            histo_2 = root_file_2.Get(histo_path)

            # clone old histogram (to get the same format)
            comp_hist = histo_1.Clone()
            # set title, write histo_1 / histo_2
            comp_hist.SetTitle(COMP_HISTO_TITLE)
            comp_hist.Divide(histo_2)
            comp_hist.SetMaximum(1.2)
            comp_hist.SetMinimum(0.8)
            comp_hist.Write("sf_comparison_"+region.lower()+"_"+variation,
                            TObject.kOverwrite)


def main(year):
    """Compare the files in a given year. year = int, like 2015."""

    # for use in naming conventions
    syear = str(year)[2:]  # like "15" from 2015

    # one scale factor file per year, find it:

    filename_1 = join(c.M2DE_OUTPUT_DIR, "SFPlots_%s_%s.root" % (
        syear, NTUPLE_VERSION_1))
    filename_2 = join(c.M2DE_OUTPUT_DIR, "SFPlots_%s_%s.root" % (
        syear, NTUPLE_VERSION_2))

    periods = ["B"]  # TODO: get_periods(year, sort=True)

    for period in periods:
        single = True  # TODO change this later!
        triggers = triggers_in_period(single, year, period)
        print(triggers)
        for trigger in triggers:
            for quality in c.WORKING_POINTS:
                dir_name = DIR_FMT.format(
                    qual=quality, prd=period, trig=trigger)
                compare(filename_1, filename_2, dir_name)


if __name__ == "__main__":
    main(2018)
