"""
Module for making 2D efficiency and scale factor plots.
Purpose:
--------

- Creating phi vs. eta efficiency plots (data and mc separately)
  - in ROOT files in format necessary for acceptance by TriggerScaleFactorTool
- **See SF tutorial for details of what efficiencies/SFs are.**

Inputs
------

The inputs for this script are the outputs of WriteTagProbeHistos (WTPH),
ntuples containing histograms 2D efficiency histograms.

To run this script, the user needs to have produced ntuples
using WriteTagProbeHistos for each systematic, for data and MC individually.

There's a specific naming convention to be followed, which will be satisfied
automatically if you run WTPH using the prepare_for_wtph.py script.

Execution:
----------

python make_2d_eff.py

Outputs:
--------

ROOT files containing 2D eta/phi efficiency plots,
for data and MC, for a number of systematics,
as well as total systematic and statistical uncertainty up/down plots.
"""
# Imports and Option Parser
from __future__ import print_function
import math
import os
import logging
from ROOT import TFile
from ROOT import TEfficiency
from ROOT import gROOT
from ROOT import TObject
from ROOT import TCanvas
from ROOT import gStyle
import core.constants as c
from core.run_numbers import get_periods
from core.triggers import triggers_in_period

logging.basicConfig()
logging.root.setLevel(logging.INFO)


# Callum note: this was here when I inherited this code,
# not really sure what this stuff was from...?
# Need to be careful about period K 2017 nvtx systematic:
# cut >/< 19 vertices for 2017 but >/< 25 for period K

# Suppresses basic info prints to terminal (used to shut up TEff constructor)
gROOT.ProcessLine("gErrorIgnoreLevel = 1001;")
# set up global style stuff
#gROOT.LoadMacro('AtlasUtils.C')
#gROOT.LoadMacro('AtlasLabels.C')
#if gROOT.LoadMacro('AtlasStyle.C') > 0:
#    SetAtlasStyle()


def draw_hist(out_dir, title_prefix, hist, name, event_numbers):
    """
    Save a histogram as a PNG.

    out_dir: string, output directory we're we'll save the file.
             This must end in _YEAR/ (e.g. _18/)
    title_prefix: e.g. v66.3.0_Medium_B_HLT_mu26_ivarmedium_etaphi_fine_endcap_
    hist: the TH2F (or similar) root histogram object
    name: e.g. dataEff_nominal / mcEff_ptup / SF_mll
    event_numbers = (n_data_probe, n_data_match) or that with mc, or all 4
    """
    def parse_prefix(title_prefix):
        """
        Helper for parsing histogram names.

        E.g. given v66.3.0_Medium_B_HLT_mu26_ivarmedium_etaphi_fine_endcap_

        Give "v66.3.0", "Medium", "B", "ptdw", "HLT_mu26_ivarmedium", "endcap"
        """
        title_prefix = str(title_prefix)  # poor-man's type hinting
        # cut off last underscore
        title_prefix = title_prefix.strip("_")

        # get the version, cut string down a little
        version = title_prefix.split("_")[0]
        title_prefix = title_prefix[len(version)+1:]

        # get quality
        quality = title_prefix.split("_")[0]
        title_prefix = title_prefix[len(quality)+1:]

        # get period
        period = title_prefix.split("_")[0]
        title_prefix = title_prefix[len(period)+1:]

        # get region from the end
        region = title_prefix.split("_")[-1]
        title_prefix = title_prefix[:-len(region)-1]

        # now looks like HLT_mu26_ivarmedium_etaphi_fine
        # get rid of the _etaphi_fine
        trigger = title_prefix.replace("_etaphi_fine", "")

        return version, quality, period, region, trigger

    # create TCanvas, set margins
    canvas = TCanvas()
    canvas.SetTopMargin(0.15)
    canvas.SetBottomMargin(0.1)
    canvas.SetLeftMargin(0.1)
    canvas.SetRightMargin(0.1)

    # format options for the bin label text I think
    gStyle.SetOptTitle(1)
    gStyle.SetOptStat(0)
    gStyle.SetPaintTextFormat(".3f")
    gStyle.SetImageScaling(3.)
    hist.SetDirectory(0)

    year = out_dir[:-1].split("_")[-1]
    version, quality, period, region, trigger = parse_prefix(title_prefix)

    plot_type = name.split("_")[0]
    variation = name[len(plot_type)+1:]

    if plot_type == "SF":
        n_data_probe, n_data_match, n_mc_probe, n_mc_match = event_numbers
        event_num_string = "n-data-probe=" + str(int(n_data_probe)) + \
            ", n-data-match=" + str(int(n_data_match)) + \
            ", n-mc-probe=" + str(int(n_mc_probe)) + \
            ", n-mc-match=" + str(int(n_mc_match))
    elif plot_type == "dataEff":
        n_data_probe, n_data_match = event_numbers
        event_num_string = "n-data-probe=" + str(int(n_data_probe)) + \
            ", n-data-match=" + str(int(n_data_match))
    elif plot_type == "mcEff":
        n_mc_probe, n_mc_match = event_numbers
        event_num_string = "n-mc-probe=" + str(int(n_mc_probe)) + \
            ", n-mc-match=" + str(int(n_mc_match))
    else:
        raise ValueError("Invalid plot_type %s", plot_type)
    title = "#splitline{"+plot_type + ": yr=" + year + ", prd=" + period + \
        ", trig=" + trigger.replace("_", "-") + \
        "}{#splitline{variation=" + variation.replace("_", "-") + \
        ", WP=" + quality + ", region=" + region + ", TPVersion=" + version +\
        "}{" + event_num_string + "}" + "}"
    hist.SetTitle(title)
    hist.SetTitleSize(0.015, "t")
    # hist.SetTitleOffset(0.5)
    gStyle.SetTitleY(1.025)
    hist.SetMaximum(1.0)
    hist.SetMinimum(0.0)

    # rotate text in endcap to make it fit in the boxes
    if "endcap" in title_prefix:
        hist.Draw("COLZ TEXT89")
    else:
        hist.Draw("COLZ TEXT")

    save_filename = out_dir + title_prefix + name + ".png"
    canvas.SaveAs(save_filename)
    canvas.Close()

def make_2d_eff_hists(year, period, region, trigger_type, trigger, quality,
                      make_sf_plots, print_sf_values, debug, save_pngs):
    """Make 2D Efficiency histograms for all the given parameters."""
    print(trigger_type, year, period, region, trigger, quality)
    # Suppresses basic info prints to terminal (used to shut up TEff constructor)
    gROOT.ProcessLine("gErrorIgnoreLevel = 10000000;")

    # check year
    if year not in c.MC_CAMPAIGNS.keys():
        raise ValueError(
            "Invalid year: "+year+" Use one of", c.MC_CAMPAIGNS.keys())

    # check period
    number_year = int("20"+year)
    if period not in get_periods(number_year):
        raise ValueError(
            "Invalid period: "+period+" Use one of", get_periods(number_year))

    # check region
    if region not in c.DETECTOR_REGIONS:
        raise ValueError(
            "Invalid detector region: "+region+" Use one of",
            c.DETECTOR_REGIONS)

    # check trigger_type
    if trigger_type not in c.TRIGGER_TYPES:
        raise ValueError(
            "Invalid trigger type:"+trigger_type+" Use one of",
            c.TRIGGER_TYPES)

    # check trigger
    single = (trigger_type == "SingleMuonTriggers")
    if trigger not in triggers_in_period(single, number_year, period):
        raise ValueError(
            "Invalid trigger for this year+period: "+trigger+" Use one of",
            triggers_in_period(single, number_year, period))

    # check quality working point
    if quality not in c.WORKING_POINTS:
        raise ValueError(
            "Invalid quality working point: "+quality+" Use one of",
            c.WORKING_POINTS)

    # check input/output dirs exist
    if not os.path.exists(c.M2DE_INPUT_DIR):
        raise ValueError("Input directory does not exist!", c.M2DE_INPUT_DIR)
    if not os.path.exists(c.M2DE_OUTPUT_DIR):
        raise ValueError("Output directory does not exist!", c.M2DE_OUTPUT_DIR)

    assert isinstance(make_sf_plots, bool)
    assert isinstance(print_sf_values, bool)
    assert isinstance(debug, bool)
    assert isinstance(save_pngs, bool)

    montecarlo = c.MC_CAMPAIGNS[year]
    # Load input files
    logging.debug("Looking for input files in directory: %s", c.M2DE_INPUT_DIR)
    input_files = os.listdir(c.M2DE_INPUT_DIR)
    logging.debug("Found %s files in %s", len(input_files), c.M2DE_INPUT_DIR)

    dir_fmt = "ZmumuTPMerged/"+quality+"MuonProbes_"+\
        region.capitalize()+"/OC/"+trigger+"/"
    probe_dir = dir_fmt+"Probe/"
    match_dir = dir_fmt+"Match/"

    # Hists to look for in the above directories
    # (also identically named) in every WTPH output)
    hist_fmt = str(
        quality+"MuonProbes_"+region.capitalize()+"_OC_"+
        trigger+"_{}_etaphi_fine_"+region.capitalize())
    probe_hist = hist_fmt.format("Probe")
    match_hist = hist_fmt.format("Match")

    logging.debug(
        "Will look in directory,\n%s\nFor hist,\n%s", probe_dir, probe_hist)

    var_wtph_outfile = "{data_mc}20"+str(year)+"_"+"_".join(
        [period, "{variation}", c.NTUPLE_VERSION, trigger_type])+"_2D.root"
    var_wtph_out_fmt = os.path.join(c.M2DE_INPUT_DIR, var_wtph_outfile)
    for d_mc in ["data", "mc"]:
        for var in c.VARIATIONS[c.NTUPLE_VERSION]:
            var_conf = var_wtph_out_fmt.format(data_mc=d_mc, variation=var)
            if not os.path.exists(var_conf):
                raise ValueError("File not found:", var_conf)

    # Unfortunately it seems necessary to keep these separately named variables
    # or else ROOT has some problem with namespaces? Not quite sure...
    # Sorry about that, future code friend
    # (and high five if you find a way to do that!)
    logging.debug(
        "Using nominal data file %s",
        var_wtph_out_fmt.format(data_mc="data", variation="nominal"))

    data_nominal = TFile(var_wtph_out_fmt.format(
        data_mc="data", variation="nominal"))

    data_dphill = TFile(var_wtph_out_fmt.format(
        data_mc="data", variation="dphill"))
    data_mll = TFile(var_wtph_out_fmt.format(
        data_mc="data", variation="mll"))
    data_muneg = TFile(var_wtph_out_fmt.format(
        data_mc="data", variation="muneg"))
    data_mupos = TFile(var_wtph_out_fmt.format(
        data_mc="data", variation="mupos"))
    data_noIP = TFile(var_wtph_out_fmt.format(
        data_mc="data", variation="noIP"))
    data_nvtx_dw = TFile(var_wtph_out_fmt.format(
        data_mc="data", variation="nvtx_dw"))
    data_nvtx_up = TFile(var_wtph_out_fmt.format(
        data_mc="data", variation="nvtx_up"))
    data_ptdw = TFile(var_wtph_out_fmt.format(
        data_mc="data", variation="ptdw"))
    data_ptup = TFile(var_wtph_out_fmt.format(
        data_mc="data", variation="ptup"))
    if c.RELEASE == 22:
        data_isoPflowLoose_VarRad = TFile(var_wtph_out_fmt.format(
            data_mc="data", variation="isoPflowLoose_VarRad"))
        data_isoPflowTight_VarRad = TFile(var_wtph_out_fmt.format(
            data_mc="data", variation="isoPflowTight_VarRad"))
        data_isoLoose_VarRad = TFile(var_wtph_out_fmt.format(
            data_mc="data", variation="isoLoose_VarRad"))
        data_isoTight_VarRad = TFile(var_wtph_out_fmt.format(
            data_mc="data", variation="isoTight_VarRad"))
        data_files = [
            data_nominal, data_dphill, data_mll, data_muneg, data_mupos,
            data_noIP, data_nvtx_dw, data_nvtx_up, data_ptdw, data_ptup,
            data_isoPflowLoose_VarRad, data_isoPflowTight_VarRad,
            data_isoLoose_VarRad, data_isoTight_VarRad
            ]
    elif c.RELEASE == 21:
        data_isoTightTrackOnly = TFile(var_wtph_out_fmt.format(
            data_mc="data", variation="isoTightTrackOnly"))
        data_isoTight = TFile(var_wtph_out_fmt.format(
            data_mc="data", variation="isoTight"))
        data_files = [
            data_nominal, data_dphill, data_mll, data_muneg, data_mupos,
            data_noIP, data_nvtx_dw, data_nvtx_up, data_ptdw, data_ptup,
            data_isoTightTrackOnly, data_isoTight
            ]
    else:
        raise ValueError("Unsupported release %s" % c.RELEASE)

    logging.debug(
        "Using nominal MC file %s",
        var_wtph_out_fmt.format(data_mc="mc", variation="nominal"))
    mc_nominal = TFile(var_wtph_out_fmt.format(
        data_mc="mc", variation="nominal"))
    mc_dphill = TFile(var_wtph_out_fmt.format(
        data_mc="mc", variation="dphill"))
    mc_mll = TFile(var_wtph_out_fmt.format(
        data_mc="mc", variation="mll"))
    mc_muneg = TFile(var_wtph_out_fmt.format(
        data_mc="mc", variation="muneg"))
    mc_mupos = TFile(var_wtph_out_fmt.format(
        data_mc="mc", variation="mupos"))
    mc_noIP = TFile(var_wtph_out_fmt.format(
        data_mc="mc", variation="noIP"))
    mc_nvtx_dw = TFile(var_wtph_out_fmt.format(
        data_mc="mc", variation="nvtx_dw"))
    mc_nvtx_up = TFile(var_wtph_out_fmt.format(
        data_mc="mc", variation="nvtx_up"))
    mc_ptdw = TFile(var_wtph_out_fmt.format(
        data_mc="mc", variation="ptdw"))
    mc_ptup = TFile(var_wtph_out_fmt.format(
        data_mc="mc", variation="ptup"))
    if c.RELEASE == 22:
        mc_isoPflowLoose_VarRad = TFile(var_wtph_out_fmt.format(
            data_mc="mc", variation="isoPflowLoose_VarRad"))
        mc_isoPflowTight_VarRad = TFile(var_wtph_out_fmt.format(
            data_mc="mc", variation="isoPflowTight_VarRad"))
        mc_isoLoose_VarRad = TFile(var_wtph_out_fmt.format(
            data_mc="mc", variation="isoLoose_VarRad"))
        mc_isoTight_VarRad = TFile(var_wtph_out_fmt.format(
            data_mc="mc", variation="isoTight_VarRad"))
        mc_files = [
            mc_nominal, mc_dphill, mc_mll, mc_muneg, mc_mupos,
            mc_noIP, mc_nvtx_dw, mc_nvtx_up, mc_ptdw, mc_ptup,
            mc_isoPflowLoose_VarRad, mc_isoPflowTight_VarRad,
            mc_isoLoose_VarRad, mc_isoTight_VarRad
            ]
    elif c.RELEASE == 21:
        mc_isoTightTrackOnly = TFile(var_wtph_out_fmt.format(
            data_mc="mc", variation="isoTightTrackOnly"))
        mc_isoTight = TFile(var_wtph_out_fmt.format(
            data_mc="mc", variation="isoTight"))
        mc_files = [
            mc_nominal, mc_dphill, mc_mll, mc_muneg, mc_mupos,
            mc_noIP, mc_nvtx_dw, mc_nvtx_up, mc_ptdw, mc_ptup,
            mc_isoTightTrackOnly, mc_isoTight
            ]
    else:
        raise ValueError("Unsupported release %s" % c.RELEASE)

    # print number of entries in nominal histograms
    n_data_nom = data_nominal.Get(probe_dir + probe_hist).GetEntries()
    n_mc_nom = mc_nominal.Get(probe_dir + probe_hist).GetEntries()
    logging.info("Number of entries, nominal probe data hist: %s", n_data_nom)
    logging.info("Number of entries, nominal probe MC hist: %s", n_mc_nom)
    logging.info("Number of entries, nominal match data hist: %s",
                 data_nominal.Get(match_dir + match_hist).GetEntries())
    logging.info("Number of entries, nominal match MC hist: %s",
                 mc_nominal.Get(match_dir + match_hist).GetEntries())

    if n_data_nom == 0:
        raise ValueError("No data entries in the nominal file!")
    if n_mc_nom == 0:
        raise ValueError("No MC entries in the nominal file!")

    for data_f in data_files:
        #print(data_f)
        #print(probe_dir + probe_hist)
        #print(data_f.Get(probe_dir + probe_hist))

        n_data_f = data_f.Get(probe_dir + probe_hist).GetEntries()
        if n_data_f == 0:
            raise ValueError("No data entries in one of the variation files!")

    for mc_f in mc_files:
        n_mc_f = mc_f.Get(probe_dir + probe_hist).GetEntries()
        if n_mc_f == 0:
            raise ValueError("No mc entries in one of the variation files!")

    probe_hists = {"data": {}, "mc": {}}
    match_hists = {"data": {}, "mc": {}}
    effs = {"data": {}, "mc": {}}
    hists = {"data": {}, "mc": {}}

    # Data:
    for i, var_file in enumerate(data_files):
        name = c.VARIATIONS[c.NTUPLE_VERSION][i]
        probe_hists["data"][name] = var_file.Get(probe_dir + probe_hist)
        if not probe_hists["data"][name]:
            raise ValueError(
                "Couldn't find %s. Does this really exist in %s?\n" %\
                (probe_dir + probe_hist, var_file.GetName()))
        match_hists["data"][name] = var_file.Get(match_dir + match_hist)
        if not match_hists["data"][name]:
            raise ValueError(
                "Couldn't find %s. Does this really exist in %s?\n" %\
                (match_dir + match_hist, var_file.GetName()))
        logging.debug("Got data probe & match hists for %s", name)

        # Make TEfficiency object to do stat errors properly
        effs["data"][name] = TEfficiency(
            match_hists["data"][name], probe_hists["data"][name])

        # Make empty TH2 which will be filled with TEff bin values
        # (+/- errors if needed) down below
        hists["data"][name] = probe_hists["data"][name].Clone()
        hists["data"][name].Reset()

    # MC:
    for j, mc_varfile in enumerate(mc_files):
        name = c.VARIATIONS[c.NTUPLE_VERSION][j]
        probe_hists["mc"][name] = mc_varfile.Get(probe_dir + probe_hist)
        if not probe_hists["mc"][name]:
            raise ValueError(
                "Couldn't find {}. Does this really exist in {}?\n".format(
                    probe_dir + probe_hist, mc_varfile.GetName()))
        match_hists["mc"][name] = mc_varfile.Get(match_dir + match_hist)
        if not match_hists["mc"][name]:
            raise ValueError(
                "Couldn't find {}. Does this really exist in {}?\n".format(
                    match_dir + match_hist, mc_varfile.GetName()))
        logging.debug("Got MC probe & match hists for %s", name)
        # Make TEff object to do stat errors properly
        effs["mc"][name] = TEfficiency(
            match_hists["mc"][name], probe_hists["mc"][name])
        #mcEffs[name].SetStatisticOption(TEfficiency.kFNormal)
        # Make empty TH2 which will be filled with TEff bin values
        # (+/- errors if needed) down below
        hists["mc"][name] = probe_hists["mc"][name].Clone()
        hists["mc"][name].Reset()

    # Setting up data systematic, statistical variation histograms
    # for data
    data_probe_nominal = probe_hists["data"]["nominal"]
    data_syst_up = data_probe_nominal.Clone('dSystUp')
    data_syst_up.Reset()
    data_syst_dw = data_probe_nominal.Clone('dSystDw')
    data_syst_dw.Reset()
    data_stat_up = data_probe_nominal.Clone('dStatUp')
    data_stat_up.Reset()
    data_stat_dw = data_probe_nominal.Clone('dStatDw')
    data_stat_dw.Reset()
    # isoEnv = isolation envelope (see calculation later in the code)
    hists["data"]["isoEnv"] = data_probe_nominal.Clone('isoEnv')
    hists["data"]["isoEnv"].Reset()
    # MC:
    mc_probe_nominal = probe_hists["mc"]["nominal"]
    mc_syst_up = mc_probe_nominal.Clone('mcSystUp')
    mc_syst_up.Reset()
    mc_syst_dw = mc_probe_nominal.Clone('mcSystDw')
    mc_syst_dw.Reset()
    mc_stat_up = mc_probe_nominal.Clone('mcStatUp')
    mc_stat_up.Reset()
    mc_stat_dw = mc_probe_nominal.Clone('mcStatDw')
    mc_stat_dw.Reset()
    hists["mc"]["isoEnv"] = mc_probe_nominal.Clone('isoEnv')
    hists["mc"]["isoEnv"].Reset()

    # Number of x- and y-bins - will be used many times
    xbins = data_probe_nominal.GetNbinsX()
    ybins = data_probe_nominal.GetNbinsY()
    assert xbins == mc_probe_nominal.GetNbinsX()
    assert ybins == mc_probe_nominal.GetNbinsY()

    # Initialize stat/syst up/down
    if make_sf_plots:
        logging.debug("makeSFPlots = True! Will initialize SF root hists")
        sf_hists = {}
        for k in hists["data"]:
            sf_hists[k] = hists["data"][k].Clone()
            sf_hists[k].Reset()
        sf_stat_up = data_stat_up.Clone()
        sf_stat_dw = data_stat_dw.Clone()
        sf_syst_up = data_syst_up.Clone()
        sf_syst_dw = data_syst_dw.Clone()

    # Initlaize SF value variables and obtain inclusive stat uncertainty
    if print_sf_values:
        logging.debug(
            "printSFValues = True! Will keep track of SF values for each syst")
        # To get inclusive SF values,
        # need to add probes and matches across all bins
        n_data_matches = {}
        n_data_probes = {}
        n_mc_matches = {}
        n_mc_probes = {}
        sf_values = {}
        for k in hists["data"]:
            n_data_matches[k] = 0
            n_data_probes[k] = 0
            n_mc_matches[k] = 0
            n_mc_probes[k] = 0
            sf_values[k] = 0
        sf_values["TotSyst"] = 0

        # Inclusive SF stat uncertainty using 1-bin copies of data/mc hists
        #Data inclusive eff
        data_match_nominal = match_hists["data"]["nominal"]
        data_match_1_bin = data_match_nominal.Clone()
        data_match_1_bin.Rebin2D(xbins, ybins)
        data_probe_1_bin = data_probe_nominal.Clone()
        data_probe_1_bin.Rebin2D(xbins, ybins)
        data_eff_1_bin = TEfficiency(data_match_1_bin, data_probe_1_bin)

        data_1_bin = hists["data"]["nominal"].Clone()
        data_1_bin.Reset()
        data_1_bin.Rebin2D(xbins, ybins)
        bin_id = data_eff_1_bin.GetGlobalBin(1, 1)
        data_eff = data_eff_1_bin.GetEfficiency(bin_id)
        data_stat_err = data_eff_1_bin.GetEfficiencyErrorUp(bin_id)

        #MC inclusive eff
        mc_match_nominal = match_hists["mc"]["nominal"]
        mc_match_1_bin = mc_match_nominal.Clone()
        mc_match_1_bin.Rebin2D(xbins, ybins)
        mc_probe_1_bin = mc_probe_nominal.Clone()
        mc_probe_1_bin.Rebin2D(xbins, ybins)
        mc_eff_1_bin = TEfficiency(mc_match_1_bin, mc_probe_1_bin)

        mc_1_bin = hists["mc"]["nominal"].Clone()
        mc_1_bin.Reset()
        mc_1_bin.Rebin2D(xbins, ybins)
        mc_eff = mc_eff_1_bin.GetEfficiency(bin_id)
        mc_stat_err = mc_eff_1_bin.GetEfficiencyErrorUp(bin_id)

        # Error propagation:
        # err_sf = sf*(err_data_eff/data_eff + err_mc_eff/mc_eff)
        if mc_eff == 0:
            raise ValueError("MC efficiency is zero -- unfilled nominal match histo")
        if data_eff == 0:
            raise ValueError("data efficiency is zero -- unfilled nominal match histo")

        scale_factor = data_eff / mc_eff
        sfstaterr = (scale_factor * (
            data_stat_err / data_eff + mc_stat_err / mc_eff))

    # Looping through each bin of each histogram
    # to grab the nominal efficiency and the systematic variations
    logging.debug('Now starting loop over bins...')
    for i in range(1, xbins + 2):
        for j in range(1, ybins + 2):
            # Debugging particular bin issues
            # print("Bin : ("+str(i)+","+str(j)+")")

            # Global bin # to used w GetEfficiency()
            # and GetEfficiencyErrorUp/Low()
            bin_id = effs["data"]["nominal"].GetGlobalBin(i, j)
            # Getting data, MC nominal efficiency and up/down stat bin errors:
            dnom = effs["data"]["nominal"].GetEfficiency(bin_id)
            dstatup = effs["data"]["nominal"].GetEfficiencyErrorUp(bin_id)
            dstatdw = effs["data"]["nominal"].GetEfficiencyErrorLow(bin_id)
            mcnom = effs["mc"]["nominal"].GetEfficiency(bin_id)
            mcstatup = effs["mc"]["nominal"].GetEfficiencyErrorUp(bin_id)
            mcstatdw = effs["mc"]["nominal"].GetEfficiencyErrorLow(bin_id)

            # Fill nominal + statUp/statDw data, MC efficiency TH2's
            hists["data"]["nominal"].SetBinContent(i, j, dnom)
            hists["mc"]["nominal"].SetBinContent(i, j, mcnom)
            data_stat_up.SetBinContent(i, j, dnom + dstatup)
            logging.debug(
                "bin %s %s mc stat up %s %s", i, j, mcnom, mcstatup)
            mc_stat_up.SetBinContent(i, j, mcnom + mcstatup)
            data_stat_dw.SetBinContent(i, j, dnom - dstatdw)
            mc_stat_dw.SetBinContent(i, j, mcnom - dstatdw)

            # Error propagation for SF stat up/down hists, in each bin
            if make_sf_plots:
                if mcnom != 0 and dnom != 0:
                    sfbinerrup = (dnom/mcnom) * (dstatup/dnom + mcstatup/mcnom)
                    sf_stat_up.SetBinContent(i, j, dnom/mcnom + sfbinerrup)
                    sfbinerrdw = (dnom/mcnom) * (dstatdw/dnom + mcstatdw/mcnom)
                    sf_stat_dw.SetBinContent(i, j, dnom/mcnom - sfbinerrdw)
                else:
                    # some bins are expected to be zero (i.e. the edges)
                    # this makes things difficult to debug sometimes...
                    logging.debug(
                        "bin %s %s, "
                        "data or MC nominal efficiency is 0... "
                        "Setting bin to zero!", i, j)
                    sf_stat_up.SetBinContent(i, j, 0)
                    sf_stat_dw.SetBinContent(i, j, 0)

            # Getting data, MC up/down systematic bin errors
            # Ints to keep track of systematic variations summed in quadrature:
            dsyst_tot = 0
            mcsyst_tot = 0

            # Isolation envelope systematic for the bin:
            # take the envelope of these variables,
            # i.e. use the max absolute difference from nominal
            if c.RELEASE == 22:
                iso_params = [
                    "isoTight_VarRad",
                    "isoPflowTight_VarRad",
                    "isoLoose_VarRad",
                    "isoPflowLoose_VarRad"
                ]
            elif c.RELEASE == 21:
                iso_params = [
                    "isoTight",
                    "isoTightTrackOnly"
                ]
            else:
                raise ValueError("Unsupported release %s" % c.RELEASE)

            deffs = effs["data"]
            dsysts = [deffs[p].GetEfficiency(bin_id) for p in iso_params]
            d_diffs = [dnom - dsyst for dsyst in dsysts]
            abs_d_diffs = [abs(diff) for diff in d_diffs]
            max_abs_d_diff = max(abs_d_diffs)
            d_max_index = abs_d_diffs.index(max_abs_d_diff)
            dsyst_iso = d_diffs[d_max_index]
            hists["data"]["isoEnv"].SetBinContent(i, j, dsysts[d_max_index])
            # Add iso syst in quadrature to total syst bin error
            dsyst_tot += (dsyst_iso**2)

            # MC
            mceffs = effs["mc"]
            mcsysts = [mceffs[p].GetEfficiency(bin_id) for p in iso_params]
            mc_diffs = [mcnom - mcsyst for mcsyst in mcsysts]
            abs_mc_diffs = [abs(diff) for diff in mc_diffs]
            max_abs_mc_diff = max(abs_mc_diffs)
            mc_max_index = abs_mc_diffs.index(max_abs_mc_diff)
            mcsyst_iso = mc_diffs[mc_max_index]
            hists["mc"]["isoEnv"].SetBinContent(i, j, mcsysts[mc_max_index])
            # Add iso syst in quadrature to total syst bin error
            mcsyst_tot += (mcsyst_iso**2)

            if print_sf_values:
                iso_var = iso_params[mc_max_index]
                n_data_matches["isoEnv"] += match_hists["data"][
                    iso_var].GetBinContent(i, j)
                n_data_probes["isoEnv"] += probe_hists["data"][
                    iso_var].GetBinContent(i, j)
                n_mc_matches["isoEnv"] += match_hists["mc"][
                    iso_var].GetBinContent(i, j)
                n_mc_probes["isoEnv"] += probe_hists["mc"][
                    iso_var].GetBinContent(i, j)

            # Get non-isolation systematics for the bin:
            for k in hists["data"]:
                if k not in ["nominal", "isoEnv"]:
                    #Data:
                    dsyst = effs["data"][k].GetEfficiency(bin_id)
                    hists["data"][k].SetBinContent(i, j, dsyst)
                    if not k.startswith("iso"):
                        dsyst_tot += ((dnom - dsyst)**2)
                    #MC:
                    mcsyst = effs["mc"][k].GetEfficiency(bin_id)
                    hists["mc"][k].SetBinContent(i, j, mcsyst)
                    if not k.startswith("iso"):
                        mcsyst_tot += ((mcnom - mcsyst)**2)
                # SF values
                if print_sf_values and k != "isoEnv":
                    n_data_matches[k] += match_hists[
                        "data"][k].GetBinContent(i, j)
                    n_data_probes[k] += probe_hists[
                        "data"][k].GetBinContent(i, j)
                    n_mc_matches[k] += match_hists[
                        "mc"][k].GetBinContent(i, j)
                    n_mc_probes[k] += probe_hists[
                        "mc"][k].GetBinContent(i, j)

            # Finally, set the bin content of the
            # Syst Up/Dw plots = nominal efficiency + total syst variation
            data_syst_up.SetBinContent(i, j, dnom + math.sqrt(dsyst_tot))
            data_syst_dw.SetBinContent(i, j, dnom - math.sqrt(dsyst_tot))
            mc_syst_up.SetBinContent(i, j, mcnom + math.sqrt(mcsyst_tot))
            mc_syst_dw.SetBinContent(i, j, mcnom - math.sqrt(mcsyst_tot))

            # Fill syst up/down plots
            if make_sf_plots:
                if mcnom != 0 and math.sqrt(mcsyst_tot) != 0:
                    sf_syst_up.SetBinContent(
                        i, j, (dnom + math.sqrt(dsyst_tot)) / (
                            mcnom + math.sqrt(mcsyst_tot)))
                    sf_syst_dw.SetBinContent(
                        i, j, (dnom - math.sqrt(dsyst_tot)) / (
                            mcnom - math.sqrt(mcsyst_tot)))
                else:
                    sf_syst_up.SetBinContent(i, j, 0)
                    sf_syst_dw.SetBinContent(i, j, 0)
                for k in hists["data"]:
                    sf_hists[k] = hists["data"][k].Clone()
                    sf_hists[k].Divide(hists["mc"][k])

    # If inclusive SF values requested,
    # get them by dividing total matches/probes for data and mc
    if print_sf_values:
        for k in hists["data"]:
            if 0 in [n_data_probes[k], n_mc_probes[k], n_mc_matches[k]]:
                raise ValueError("No entries in one of your histograms!")
            else:
                sf_values[k] = (n_data_matches[k] / n_data_probes[k]) / (
                    n_mc_matches[k] / n_mc_probes[k])

    # Create/update systematics Efficiencies TFile
    # make efficiency ROOT files
    if debug:
        effs_filepath = os.path.join(c.M2DE_OUTPUT_DIR, "debug.root")
        # test output file
    else:
        # Change ntuple version to match your inputs!
        # outfile named in format for SF tool
        effs_filepath = os.path.join(
            c.M2DE_OUTPUT_DIR, 'muontrigger_sf_20%s_mc%s_%s.root' %
            (year, montecarlo, c.NTUPLE_VERSION))
    logging.info("Will output data, MC efficiencies to %s", effs_filepath)
    effs_file = TFile(effs_filepath, 'update')
    # Create directory
    # (trigger may or may not contain _RM, replace does nothing if not)
    dir_name = quality + "/Period" + period + "/" +\
        trigger.replace("_RM", "") + "/"

    logging.debug(" - Directory: %s", dir_name)
    effs_file.mkdir(dir_name)
    effs_file.cd(dir_name)

    # Put corresponding plots into working point directory

    # Write Data efficiency histograms:
    hists["data"]["nominal"].Write("eff_etaphi_fine_%s_data_nominal" % (
        region.lower()), TObject.kOverwrite)
    data_syst_up.Write("eff_etaphi_fine_%s_data_syst_up" % (
        region.lower()), TObject.kOverwrite)
    data_syst_dw.Write("eff_etaphi_fine_%s_data_syst_down" % (
        region.lower()), TObject.kOverwrite)
    data_stat_up.Write("eff_etaphi_fine_%s_data_stat_up" % (
        region.lower()), TObject.kOverwrite)
    data_stat_dw.Write("eff_etaphi_fine_%s_data_stat_down" % (
        region.lower()), TObject.kOverwrite)
    for k in sorted(hists["data"].keys()):
        if k != "nominal":
            hists["data"][k].Write("eff_etaphi_fine_%s_data_%s" % (
                region.lower(), k), TObject.kOverwrite)

    # Write MC efficiency histograms:
    hists["mc"]["nominal"].Write("eff_etaphi_fine_%s_mc_nominal" % (
        region.lower()), TObject.kOverwrite)
    mc_syst_up.Write("eff_etaphi_fine_%s_mc_syst_up" % (
        region.lower()), TObject.kOverwrite)
    mc_syst_dw.Write("eff_etaphi_fine_%s_mc_syst_down" % (
        region.lower()), TObject.kOverwrite)
    mc_stat_up.Write("eff_etaphi_fine_%s_mc_stat_up" % (
        region.lower()), TObject.kOverwrite)
    mc_stat_dw.Write("eff_etaphi_fine_%s_mc_stat_down" % (
        region.lower()), TObject.kOverwrite)
    for k in sorted(hists["mc"].keys()):
        if k != "nominal":
            hists["mc"][k].Write("eff_etaphi_fine_%s_mc_%s" % (
                region.lower(), k), TObject.kOverwrite)

    # Save all data, mc (and SF, if SF plots made) hists as pngs
    if save_pngs:
        # Directory
        png_outdir = os.path.join(c.M2DE_OUTPUT_DIR, "savePNGs_%s/" % (year))
        logging.info("savePNGs = True! Will save PNGs to: %s", png_outdir)
        if not os.path.exists(png_outdir):
            os.mkdir(png_outdir)
        # prefix for hist titles
        title_prefix = "%s_%s_%s_%s_etaphi_fine_%s_" % (
            c.NTUPLE_VERSION, quality, period,
            trigger.replace("_RM", ""), region.lower())
        gROOT.SetBatch()

        # Data stat/syst up/down
        # use nominal for event numbers for these odd ones
        n_data_probe = data_nominal.Get(probe_dir + probe_hist).GetEntries()
        n_data_match = data_nominal.Get(match_dir + match_hist).GetEntries()
        n_mc_probe = mc_nominal.Get(probe_dir + probe_hist).GetEntries()
        n_mc_match = mc_nominal.Get(match_dir + match_hist).GetEntries()

        draw_hist(png_outdir, title_prefix, data_syst_up, "dataEff_syst_up",
                  event_numbers=(n_data_probe, n_data_match))
        draw_hist(png_outdir, title_prefix, data_syst_dw, "dataEff_syst_dw",
                  event_numbers=(n_data_probe, n_data_match))
        draw_hist(png_outdir, title_prefix, data_stat_up, "dataEff_stat_up",
                  event_numbers=(n_data_probe, n_data_match))
        draw_hist(png_outdir, title_prefix, data_stat_dw, "dataEff_stat_dw",
                  event_numbers=(n_data_probe, n_data_match))
        # MC stat/syst up/down
        draw_hist(png_outdir, title_prefix, mc_syst_up, "mcEff_syst_up",
                  event_numbers=(n_mc_probe, n_mc_match))
        draw_hist(png_outdir, title_prefix, mc_syst_dw, "mcEff_syst_dw",
                  event_numbers=(n_mc_probe, n_mc_match))
        draw_hist(png_outdir, title_prefix, mc_stat_up, "mcEff_stat_up",
                  event_numbers=(n_mc_probe, n_mc_match))
        draw_hist(png_outdir, title_prefix, mc_stat_dw, "mcEff_stat_dw",
                  event_numbers=(n_mc_probe, n_mc_match))
        # SF stat/syst up/down
        if make_sf_plots:
            draw_hist(png_outdir, title_prefix, sf_syst_up, "SF_syst_up",
                  (n_data_probe, n_data_match, n_mc_probe, n_mc_match))
            draw_hist(png_outdir, title_prefix, sf_syst_dw, "SF_syst_dw",
                  (n_data_probe, n_data_match, n_mc_probe, n_mc_match))
            draw_hist(png_outdir, title_prefix, sf_stat_up, "SF_stat_up",
                  (n_data_probe, n_data_match, n_mc_probe, n_mc_match))
            draw_hist(png_outdir, title_prefix, sf_stat_dw, "SF_stat_dw",
                  (n_data_probe, n_data_match, n_mc_probe, n_mc_match))
        # Everything else
        for k in hists["data"]:
            if k == "isoEnv":
                # special case for this one
                n_data_probe = data_nominal.Get(
                    probe_dir + probe_hist).GetEntries()
                n_data_match = data_nominal.Get(
                    match_dir + match_hist).GetEntries()
                n_mc_probe = mc_nominal.Get(
                    probe_dir + probe_hist).GetEntries()
                n_mc_match = mc_nominal.Get(
                    match_dir + match_hist).GetEntries()
            else:
                n_data_probe = locals()["data_"+k].Get(
                    probe_dir + probe_hist).GetEntries()
                n_data_match = locals()["data_"+k].Get(
                    match_dir + match_hist).GetEntries()
                n_mc_probe = locals()["mc_"+k].Get(
                    probe_dir + probe_hist).GetEntries()
                n_mc_match = locals()["mc_"+k].Get(
                    match_dir + match_hist).GetEntries()
            draw_hist(
                png_outdir, title_prefix,
                hists["data"][k], "dataEff_{}".format(k),
                event_numbers=(n_data_probe, n_data_match))
            draw_hist(
                png_outdir, title_prefix,
                hists["mc"][k], "mcEff_{}".format(k),
                event_numbers=(n_mc_probe, n_mc_match))
            if make_sf_plots:
                draw_hist(
                    png_outdir, title_prefix,
                    sf_hists[k], "SF_{}".format(k),
                    (n_data_probe, n_data_match, n_mc_probe, n_mc_match))

    # Create separate SF TFile
    if make_sf_plots:
        if debug:
            sf_filename = "debug_SF.root"
        else:
            sf_filename = "SFPlots_%s_%s.root" % (year, c.NTUPLE_VERSION)
        sf_filepath = os.path.join(c.M2DE_OUTPUT_DIR, sf_filename)
        logging.info(" ---> Will output SFs to file %s", sf_filepath)
        sf_file = TFile(sf_filepath, 'update')
        dir_name = quality + "/Period" + period + "/" + trigger + "/"
        logging.debug(" - Directory: %s", dir_name)
        sf_file.mkdir(dir_name)
        sf_file.cd(dir_name)
        sf_hists["nominal"].Write("sf_%s_nominal" % (region.lower()))
        sf_syst_up.Write("sf_%s_syst_up" % (
            region.lower()), TObject.kOverwrite)
        sf_syst_dw.Write("sf_%s_syst_down" % (
            region.lower()), TObject.kOverwrite)
        sf_stat_up.Write("sf_%s_stat_up" % (
            region.lower()), TObject.kOverwrite)
        sf_stat_dw.Write("sf_%s_stat_down" % (
            region.lower()), TObject.kOverwrite)
        for k in sorted(sf_hists.keys()):
            if k != "nominal":
                sf_hists[k].Write("sf_%s_%s" % (
                    region.lower(), k), TObject.kOverwrite)

    # Print inclusive SF for nominal, systematic plots
    # print to a file, that is.
    if print_sf_values:
        text = ""
        vars_to_show = [
            var for var in c.VARIATIONS[c.NTUPLE_VERSION]
            if not ("iso" in var and var != "isoEnv")]

        spacing = max(
            [len(syst) for syst in vars_to_show]) + 1
        text += "Scale Factor Values: " + ", ".join([
            str(number_year), period]) + "\n"
        text += ", ".join([trigger, region, quality]) + "\n"
        tot_stat_var = 0
        text += "{:<{spacing}} {:<8} {}".format(
            'Systematic', 'Value', '% Diff', spacing=spacing) + "\n"
        text += \
            "{:<{spacing}} {:<8} {:<8} {}".format(
            "nominal", round(sf_values["nominal"], 5), "N/A",
            "Stat Error: " + str(round(sfstaterr, 5)),
            spacing=spacing) + "\n"

        for k, sf_val in sorted(sf_values.items()):
            # skip nominal, TotSyst, and non-isoEnv isolation systematics
            if k == "nominal" or k == "TotSyst":
                continue
            if "iso" in k and k != "isoEnv":
                continue
            tot_stat_var += (sf_val - sf_values["nominal"])**2
            sf_rounded = round(sf_val, 5)
            if sf_values["nominal"] == 0:
                pct_diff = -1
            else:
                pct_diff = round(
                    (sf_val - sf_values["nominal"]) / sf_values["nominal"]
                    * 100, 5)
            text += "{:<{spacing}} {:<8} {}".format(
                k, sf_rounded, pct_diff, spacing=spacing) + "\n"
        sf_values["TotSyst"] = sf_values["nominal"] + math.sqrt(tot_stat_var)
        tot_sf_rounded = round(sf_values["TotSyst"], 5)
        tot_pct_diff = -1 if sf_values["nominal"] == 0 else round(
            (sf_values["TotSyst"]-sf_values["nominal"]
            )/sf_values["nominal"] * 100, 5)
        text += "{:<{spacing}} {:<8} {}".format(
            "Total", tot_sf_rounded, tot_pct_diff, spacing=spacing) + "\n"
        # output to file
        sf_filename = "sf_values/"+ "_".join([
            c.NTUPLE_VERSION, year, period, region, trigger, quality])+".txt"
        if not os.path.exists("sf_values"):
            os.mkdir("sf_values")
        with open(sf_filename, "w") as sf_file:
            sf_file.write(text)


def multi_run(trigger_types=None,
              years=None,
              periods="ALL",
              detector_regions=None,
              triggers="ALL",
              qualities=None,
              debug=False,
              make_sf_plots=True,
              print_sf_values=True,
              save_pngs=True):
    """Run make_2d_eff_hists with a bunch of different arguments."""
    if trigger_types is None:
        trigger_types = c.TRIGGER_TYPES
    if years is None:
        years = c.YEARS
    if detector_regions is None:
        detector_regions = c.DETECTOR_REGIONS
    if qualities is None:
        qualities = c.WORKING_POINTS
    # we'll set triggers again later, store if it was initially "ALL"
    all_triggers = triggers == "ALL"
    all_periods = periods == "ALL"
    all_regions = detector_regions == "ALL"
    all_qualities = qualities == "ALL"

    for trigger_type in trigger_types:
        single = trigger_type == "SingleMuonTriggers"
        for number_year in years:
            year = str(number_year)[2:]
            if all_periods:
                periods = get_periods(number_year)
            for period in periods:
                if all_regions:
                    detector_regions = c.DETECTOR_REGIONS
                for region in detector_regions:
                    if all_triggers:
                        triggers = triggers_in_period(
                            single, number_year, period)
                    for trigger in triggers:
                        if all_qualities:
                            qualities = c.WORKING_POINTS
                        for quality in qualities:
                            try:
                                make_2d_eff_hists(
                                    year, period, region, trigger_type, trigger,
                                    quality, make_sf_plots,
                                    print_sf_values, debug, save_pngs)
                            except ValueError as val_err:
                                print("ValueError:", val_err)


def main():
    """Make 2D Efficiency Histograms."""

    run_all = True

    if run_all:
        multi_run()
    else:
        years = [2018]  # c.YEARS
        periods = ["B"]  # "ALL"
        regions = ["Endcap"]  # c.DETECTOR_REGIONS
        trigger_types = ["SingleMuonTriggers"]  # c.TRIGGER_TYPES
        triggers = ["HLT_mu26_ivarmedium"]  # "ALL"
        qualities = ["Medium"]  # c.WORKING_POINTS

        multi_run(
            trigger_types=trigger_types,
            years=years,
            periods=periods,
            detector_regions=regions,
            triggers=triggers,
            qualities=qualities,
            save_pngs=True)


if __name__ == "__main__":
    main()
